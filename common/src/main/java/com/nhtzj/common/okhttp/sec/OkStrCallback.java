package com.nhtzj.common.okhttp.sec;


import android.text.TextUtils;

import com.nhtzj.common.R;

import com.nhtzj.common.util.XLog;
import com.nhtzj.common.util.ToastUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.Call;

/**
 * <pre>
 *     author : Haitao
 *     e-mail : haitao_ni@foxmail.com
 *     time   : 2017/05/16
 *     desc   : okhttp 回调返回String格式数据
 *     version: 1.0
 * </pre>
 */


public abstract class OkStrCallback extends OkCallback<String> {
    private boolean showAlert = true;

    public OkStrCallback() {
    }


    public OkStrCallback(boolean showAlert) {
        this.showAlert = showAlert;
    }


    @Override
    public void onNetError(Call call, Exception e, int id, int code) {
        if (-1 == code) {
            if (showAlert) {
                ToastUtils.makeText(R.string.tip_no_network);
            }

            onError(e, id);
            onError2(e, code, id);
            return;
        }
        if (e != null) {
            String message = e.getMessage();
            if (401 == code) {
//                UserManager.getUserManager().setTokenUnValid();
                if (showAlert) {
                    ToastUtils.makeText("请先登录");
                }

            } else if (showAlert) {
                if (!TextUtils.isEmpty(message)) {
                    try {
                        JSONObject jo = new JSONObject(message);
                        String msg = jo.optString("message");
                        if (!TextUtils.isEmpty(msg)) {
                            ToastUtils.makeText(msg);
                        }
                    } catch (JSONException e1) {
                        e1.printStackTrace();
                        ToastUtils.makeText(R.string.tip_error_network);
                    }
                } else {
                    ToastUtils.makeText(R.string.tip_error_network);
                }
            }
        } else if (showAlert) {
            ToastUtils.makeText(R.string.tip_error_network);
        }

        onError(e, id);
        onError2(e, code, id);
    }

    @Override
    public void onParseError(String response, Exception e, int id) {
        if (showAlert) {
            ToastUtils.makeText(R.string.tip_error_data_process);
        }
        onError(e, id);
        onError2(e, 2, id);
    }

    @Override
    public void onResponse(String response, int id, int code) throws Exception {
        XLog.e(id + "  " + response);
        if (TextUtils.isEmpty(response)) {
            return;
        }
        JSONObject jo = new JSONObject(response);
        if (code == 200 || code == 201) {
            if (jo.has("data")) {
                Object o = jo.opt("data");
                if (o instanceof JSONObject) {
                    onDataSuccess(o.toString());
                } else if (o instanceof JSONArray) {
                    onDataSuccess(o.toString());
                } else if (o instanceof String) {
                    onDataSuccess((String) o);
                } else if (o instanceof Boolean) {
                    onDataSuccess(o.toString());
                } else {
                    onDataSuccess(null);
                }
            } else {
                onDataSuccess(response);
            }

        } else {
            if (304 == code) {
//                UserManager.getUserManager().setTokenUnValid();
            }
            String detail = null;
            if (showAlert) {
                if (304 == code) {
                    detail = "登录过期，请重新登录";
                } else if (301 == code) {
                    detail = "请先登录";
                } else {
                    detail = jo.getString("message");
                }
                ToastUtils.makeText(detail);
            }
            onDataError(code, detail);
        }

    }


    public abstract void onDataSuccess(String dataPacket) throws Exception;

    public void onDataError(int errorCode, String errorMsg) {

    }

    public void onError(Exception e, int id) {

    }

    /**
     * 重写onError2，可获取错误码
     *
     * @param e
     * @param errCode
     * @param id
     */
    public void onError2(Exception e, int errCode, int id) {
        onAfter(id);
        XLog.e(e);

    }


}
