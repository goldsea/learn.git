package com.nhtzj.common.okhttp.utils;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.widget.Toast;

import com.nhtzj.common.util.ContextUtils;


/**
 * <pre>
 *     author : Haitao
 *     e-mail : xxx@xx
 *     time   : 2017/05/16
 *     desc   :
 *     version: 1.0
 * </pre>
 */


public class OkToastUtils {

    private static Toast mToast;

    static {
        OkToastUtils.mToast = null;
    }

    public static void destory() {
        if (OkToastUtils.mToast != null) {
            OkToastUtils.mToast = null;
        }
    }

    public static void makeText(Activity activity, final String msg) {
        if (activity == null) {
            return;
        }
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (OkToastUtils.mToast == null) {
                    OkToastUtils.mToast = Toast.makeText(ContextUtils.getContext(), (CharSequence) msg, Toast.LENGTH_SHORT);
                } else {
                    OkToastUtils.mToast.setText(msg);
                    if (msg.length() < 20) {
                        OkToastUtils.mToast.setDuration(Toast.LENGTH_SHORT);
                    } else {
                        OkToastUtils.mToast.setDuration(Toast.LENGTH_LONG);
                    }
                }
                OkToastUtils.mToast.show();
//                OkToastUtils.mToast = null;
            }
        });
    }

    @Deprecated
    public static void makeText(Context context, CharSequence msg) {
        if (msg == null) {
            return;
        }
        if (OkToastUtils.mToast == null) {
            OkToastUtils.mToast = Toast.makeText(ContextUtils.getContext(), msg, Toast.LENGTH_SHORT);
        } else {
            OkToastUtils.mToast.setText(msg);
            OkToastUtils.mToast.setDuration(Toast.LENGTH_SHORT);
        }
        OkToastUtils.mToast.show();
//        OkToastUtils.mToast = null;
    }

    public static void makeText(CharSequence text) {
        if (text == null) {
            return;
        }
        if (OkToastUtils.mToast == null) {
            OkToastUtils.mToast = Toast.makeText(ContextUtils.getContext(), text, Toast.LENGTH_SHORT);
        } else {
            OkToastUtils.mToast.setText(text);
            OkToastUtils.mToast.setDuration(Toast.LENGTH_SHORT);
        }
        OkToastUtils.mToast.show();
//        OkToastUtils.mToast = null;
    }

    public static void makeText(String msg) {
        if (msg == null) {
            return;
        }
        if (OkToastUtils.mToast == null) {
            OkToastUtils.mToast = Toast.makeText(ContextUtils.getContext(), msg, Toast.LENGTH_SHORT);
        } else {
            OkToastUtils.mToast.setText((msg));
            OkToastUtils.mToast.setDuration(Toast.LENGTH_SHORT);
        }
        OkToastUtils.mToast.show();
//        OkToastUtils.mToast = null;
    }

    public static void makeText(int msgRes) {
        if (msgRes == 0) {
            return;
        }
        if (OkToastUtils.mToast == null) {
            OkToastUtils.mToast = Toast.makeText(ContextUtils.getContext(), msgRes, Toast.LENGTH_SHORT);
        } else {
            OkToastUtils.mToast.setText(msgRes);
            OkToastUtils.mToast.setDuration(Toast.LENGTH_SHORT);
        }
        OkToastUtils.mToast.show();
//        OkToastUtils.mToast = null;
    }

    public static void makeText(final Activity activity, final int messageRes) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                CharSequence message = activity.getResources().getString(messageRes);
                if (TextUtils.isEmpty(message)) {
                    return;
                }
                if (mToast == null) {
                    mToast = Toast.makeText(activity, message, Toast.LENGTH_SHORT);
                } else {
                    mToast.setText(message);
                    if (message.length() < 20)
                        mToast.setDuration(Toast.LENGTH_SHORT);
                    else
                        mToast.setDuration(Toast.LENGTH_LONG);
                }
                mToast.show();
            }
        });
    }
}

