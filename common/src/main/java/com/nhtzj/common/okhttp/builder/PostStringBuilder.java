package com.nhtzj.common.okhttp.builder;


import com.nhtzj.common.okhttp.request.PostStringRequest;
import com.nhtzj.common.okhttp.request.RequestCall;

import okhttp3.MediaType;

public class PostStringBuilder extends OkHttpRequestBuilder<PostStringBuilder> {
    private String content;
    private MediaType mediaType;

    @Override
    public RequestCall build() {
        return new PostStringRequest(url, tag, params, headers, content, mediaType, id).build();
    }

    public PostStringBuilder content(final String content) {
        this.content = content;
        return this;
    }

    public PostStringBuilder mediaType(final MediaType mediaType) {
        this.mediaType = mediaType;
        return this;
    }
}