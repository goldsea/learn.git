package com.nhtzj.common.okhttp.callback;

/**
 * <pre>
 *     author : Haitao
 *     e-mail : xxx@xx
 *     time   : 2017/05/15
 *     desc   :
 *     version: 1.0
 * </pre>
 */


public interface IGenericsSerializator {
    <T> T transform(final String p0, final Class<T> p1);
}