package com.nhtzj.common.okhttp.callback;

import java.io.IOException;

import okhttp3.Response;

/**
 * <pre>
 *     author : Haitao
 *     e-mail : xxx@xx
 *     time   : 2017/05/15
 *     desc   :
 *     version: 1.0
 * </pre>
 */

public abstract class GenericsCallback<T> extends Callback<T> {
    IGenericsSerializator mGenericsSerializator;

    public GenericsCallback(IGenericsSerializator paramIGenericsSerializator) {
        this.mGenericsSerializator = paramIGenericsSerializator;
    }

    public T parseNetworkResponse(Response paramResponse, int paramInt)
            throws IOException {
        String str = paramResponse.body().string();
        Class localClass = (Class) ((java.lang.reflect.ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        if (localClass == String.class) {
            return (T) str;
        }
        return mGenericsSerializator.transform(str, (Class<T>) localClass);
    }
}
