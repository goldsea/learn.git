package com.nhtzj.common.okhttp.builder;

import com.nhtzj.common.okhttp.request.OtherRequest;
import com.nhtzj.common.okhttp.request.RequestCall;

import okhttp3.RequestBody;

/**
 * <pre>
 *     author : Haitao
 *     e-mail : xxx@xx
 *     time   : 2017/05/15
 *     desc   :
 *     version: 1.0
 * </pre>
 */


public class OtherRequestBuilder extends OkHttpRequestBuilder<OtherRequestBuilder> {
    private String content;
    private String method;
    private RequestBody requestBody;

    public OtherRequestBuilder(final String method) {
        this.method = method;
    }

    @Override
    public RequestCall build() {
        return new OtherRequest(requestBody, content, method, url, tag, params, headers, id).build();
    }

    public OtherRequestBuilder requestBody(final String content) {
        this.content = content;
        return this;
    }

    public OtherRequestBuilder requestBody(final RequestBody requestBody) {
        this.requestBody = requestBody;
        return this;
    }
}
