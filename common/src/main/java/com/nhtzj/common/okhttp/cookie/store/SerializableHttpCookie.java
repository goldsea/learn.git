package com.nhtzj.common.okhttp.cookie.store;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import okhttp3.Cookie;

/**
 * <pre>
 *     author : Haitao
 *     e-mail : xxx@xx
 *     time   : 2017/05/15
 *     desc   :
 *     version: 1.0
 * </pre>
 */


public class SerializableHttpCookie implements Serializable {
    private static final long serialVersionUID = 6374381323722046732L;
    private transient Cookie clientCookie;
    private final transient Cookie cookie;

    public SerializableHttpCookie(Cookie paramCookie) {
        this.cookie = paramCookie;
    }

    private void readObject(ObjectInputStream paramObjectInputStream)
            throws IOException, ClassNotFoundException {
        String str1 = (String) paramObjectInputStream.readObject();
        String str2 = (String) paramObjectInputStream.readObject();
        long l = paramObjectInputStream.readLong();
        String str3 = (String) paramObjectInputStream.readObject();
        String str4 = (String) paramObjectInputStream.readObject();
        boolean bool1 = paramObjectInputStream.readBoolean();
        boolean bool2 = paramObjectInputStream.readBoolean();
        boolean bool3 = paramObjectInputStream.readBoolean();
        paramObjectInputStream.readBoolean();
        Cookie.Builder localBuilder1 = new Cookie.Builder().name(str1).value(str2).expiresAt(l);
        if (bool3) ;
        for (Cookie.Builder localBuilder2 = localBuilder1.hostOnlyDomain(str3); ; localBuilder2 = localBuilder1.domain(str3)) {
            Cookie.Builder localBuilder3 = localBuilder2.path(str4);
            if (bool1)
                localBuilder3 = localBuilder3.secure();
            if (bool2)
                localBuilder3 = localBuilder3.httpOnly();
            this.clientCookie = localBuilder3.build();
            return;
        }
    }

    private void writeObject(ObjectOutputStream paramObjectOutputStream)
            throws IOException {
        paramObjectOutputStream.writeObject(this.cookie.name());
        paramObjectOutputStream.writeObject(this.cookie.value());
        paramObjectOutputStream.writeLong(this.cookie.expiresAt());
        paramObjectOutputStream.writeObject(this.cookie.domain());
        paramObjectOutputStream.writeObject(this.cookie.path());
        paramObjectOutputStream.writeBoolean(this.cookie.secure());
        paramObjectOutputStream.writeBoolean(this.cookie.httpOnly());
        paramObjectOutputStream.writeBoolean(this.cookie.hostOnly());
        paramObjectOutputStream.writeBoolean(this.cookie.persistent());
    }

    public Cookie getCookie() {
        Cookie localCookie = this.cookie;
        if (this.clientCookie != null)
            localCookie = this.clientCookie;
        return localCookie;
    }
}