package com.nhtzj.common.okhttp.cookie.store;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import okhttp3.Cookie;
import okhttp3.HttpUrl;

/**
 * <pre>
 *     author : Haitao
 *     e-mail : xxx@xx
 *     time   : 2017/05/15
 *     desc   :
 *     version: 1.0
 * </pre>
 */


public class MemoryCookieStore implements CookieStore {
    private final HashMap<String, List<Cookie>> allCookies;

    public MemoryCookieStore() {
        this.allCookies = new HashMap<String, List<Cookie>>();
    }

    @Override
    public void add(final HttpUrl httpUrl, final List<Cookie> list) {
        final List<Cookie> list2 = this.allCookies.get(httpUrl.host());
        if (list2 != null) {
            final Iterator<Cookie> iterator = list.iterator();
            final Iterator<Cookie> iterator2 = list2.iterator();
            while (iterator.hasNext()) {
                final String name = iterator.next().name();
                while (name != null && iterator2.hasNext()) {
                    final String name2 = iterator2.next().name();
                    if (name2 != null && name.equals(name2)) {
                        iterator2.remove();
                    }
                }
            }
            list2.addAll(list);
            return;
        }
        this.allCookies.put(httpUrl.host(), list);
    }

    @Override
    public List<Cookie> get(final HttpUrl httpUrl) {
        List<Cookie> list = this.allCookies.get(httpUrl.host());
        if (list == null) {
            list = new ArrayList<Cookie>();
            this.allCookies.put(httpUrl.host(), list);
        }
        return list;
    }

    @Override
    public List<Cookie> getCookies() {
        final ArrayList<Cookie> list = new ArrayList<Cookie>();
        final Iterator<String> iterator = this.allCookies.keySet().iterator();
        while (iterator.hasNext()) {
            list.addAll(this.allCookies.get(iterator.next()));
        }
        return list;
    }

    @Override
    public boolean remove(final HttpUrl httpUrl, final Cookie cookie) {
        final List<Cookie> list = this.allCookies.get(httpUrl.host());
        return cookie != null && list.remove(cookie);
    }

    @Override
    public boolean removeAll() {
        this.allCookies.clear();
        return true;
    }
}

