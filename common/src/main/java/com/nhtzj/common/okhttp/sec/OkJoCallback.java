package com.nhtzj.common.okhttp.sec;


import android.text.TextUtils;

import com.nhtzj.common.R;
import com.nhtzj.common.util.XLog;
import com.nhtzj.common.util.ToastUtils;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.Call;


/**
 * <pre>
 *     author : Haitao
 *     e-mail : haitao_ni@foxmail.com
 *     time   : 2017/05/16
 *     desc   : okhttp 回调返回JSONObject格式数据
 *     version: 1.0
 * </pre>
 */


public abstract class OkJoCallback extends OkCallback<JSONObject> {
    private boolean showAlert = true;

    public OkJoCallback() {
    }


    public OkJoCallback(boolean showAlert) {
        this.showAlert = showAlert;
    }


    @Override
    public void onNetError(Call call, Exception e, int id, int code) {
        if (-1 == code) {
            if (showAlert) {
                ToastUtils.makeText(R.string.tip_c_network_error);
            }

            onError(e, id);
            onError2(e, code, id);
            return;
        }

        if (e != null) {
            String message = e.getMessage();
            if (401 == code) {
//                UserManager.getUserManager().setTokenUnValid();
                if (showAlert) {
                    ToastUtils.makeText("请先登录");
                }

            } else if (showAlert) {
                if (!TextUtils.isEmpty(message)) {
                    try {
                        JSONObject jo = new JSONObject(message);
                        String msg = jo.optString("message");
                        if (!TextUtils.isEmpty(msg)) {
                            ToastUtils.makeText(msg);
                        }
                    } catch (JSONException e1) {
                        e1.printStackTrace();
                        ToastUtils.makeText(R.string.tip_error_network);
                    }
                } else {
                    ToastUtils.makeText(R.string.tip_error_network);
                }
            }
        } else if (showAlert) {
            ToastUtils.makeText(R.string.tip_error_network);
        }
        onError(e, id);
        onError2(e, code, id);
    }

    @Override
    public void onParseError(JSONObject response, Exception e, int id) {
        if (showAlert) {
            ToastUtils.makeText(R.string.tip_error_data_process);
        }
        onError(e, id);
        onError2(e, 2, id);
    }

    @Override
    public void onResponse(JSONObject response, int id, int code) throws Exception {
        XLog.e(response.toString());
        if (code == 200 || code == 201) {
            if (response.has("data")) {
                Object data = response.opt("data");
                if (data instanceof JSONObject) {
                    onDataSuccess((JSONObject) data);
                } else {
                    onDataSuccess(null);
                }
            } else {
                onDataSuccess(response);
            }
        } else {
            if (304 == code) {
//                getUserManager().setTokenUnValid();
            }
            String detail = null;
            if (showAlert) {
                if (304 == code) {
                    detail = "登录过期，请重新登录";
                } else if (301 == code) {
                    detail = "请先登录";
                } else {
                    detail = response.getString("message");
                }
                ToastUtils.makeText(detail);
            }
            onDataError(code, detail, id);
        }

    }


    public abstract void onDataSuccess(JSONObject dataPacket) throws Exception;

    public void onDataError(int errorCode, String errorMsg, int id) {
        onError(new RuntimeException("非200"), id);
        onError2(new RuntimeException("非200"), 1, id);
    }

    public void onError(Exception e, int id) {
        onAfter(id);
        XLog.e(e);

    }

    public void onError2(Exception e, int errCode, int id) {
        onAfter(id);
        XLog.e(e);

    }

    public void setShowAlert(boolean showAlert) {
        this.showAlert = showAlert;
    }
}
