package com.nhtzj.common.okhttp.request;

import android.text.TextUtils;

import com.nhtzj.common.okhttp.utils.Exceptions;

import java.util.Map;

import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.internal.http.HttpMethod;

/**
 * <pre>
 *     author : Haitao
 *     e-mail : xxx@xx
 *     time   : 2017/05/15
 *     desc   :
 *     version: 1.0
 * </pre>
 */


public class OtherRequest extends OkHttpRequest {
    private static MediaType MEDIA_TYPE_PLAIN;
    private String content;
    private String method;
    private RequestBody requestBody;

    static {
        OtherRequest.MEDIA_TYPE_PLAIN = MediaType.parse("text/plain;charset=utf-8");
    }

    public OtherRequest(final RequestBody requestBody, final String content, final String method, final String s, final Object o, final Map<String, String> map, final Map<String, String> map2, final int n) {
        super(s, o, map, map2, n);
        this.requestBody = requestBody;
        this.method = method;
        this.content = content;
    }

    @Override
    protected Request buildRequest(final RequestBody requestBody) {
        if (this.method.equals("PUT")) {
            this.builder.put(requestBody);
        } else if (this.method.equals("DELETE")) {
            if (requestBody == null) {
                this.builder.delete();
            } else {
                this.builder.delete(requestBody);
            }
        } else if (this.method.equals("HEAD")) {
            this.builder.head();
        } else if (this.method.equals("PATCH")) {
            this.builder.patch(requestBody);
        }
        return this.builder.build();
    }

    @Override
    protected RequestBody buildRequestBody() {
        if (this.requestBody == null && TextUtils.isEmpty((CharSequence) this.content) && HttpMethod.requiresRequestBody(this.method)) {
            Exceptions.illegalArgument("requestBody and content can not be null in method:" + this.method, new Object[0]);
        }
        if (this.requestBody == null && !TextUtils.isEmpty((CharSequence) this.content)) {
            this.requestBody = RequestBody.create(OtherRequest.MEDIA_TYPE_PLAIN, this.content);
        }
        return this.requestBody;
    }
}
