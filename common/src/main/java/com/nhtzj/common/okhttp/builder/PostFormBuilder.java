package com.nhtzj.common.okhttp.builder;


import com.nhtzj.common.okhttp.request.PostFormRequest;
import com.nhtzj.common.okhttp.request.RequestCall;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class PostFormBuilder extends OkHttpRequestBuilder<PostFormBuilder> implements HasParamsable {
    private List<FileInput> files;

    public PostFormBuilder() {
        this.files = new ArrayList<FileInput>();
    }

    public PostFormBuilder addFile(final String s, final String s2, final File file) {
        this.files.add(new FileInput(s, s2, file));
        return this;
    }

    @Override
    public PostFormBuilder addParams(final String key, final String value) {
        if (this.params == null) {
            this.params = new LinkedHashMap<String, String>();
        }
        this.params.put(key, value);
        return this;
    }

    @Override
    public RequestCall build() {
        return new PostFormRequest(this.url, this.tag, this.params, this.headers, this.files, this.id).build();
    }

    public PostFormBuilder files(final String s, final Map<String, File> map) {
        for (final String s2 : map.keySet()) {
            this.files.add(new FileInput(s, s2, map.get(s2)));
        }
        return this;
    }

    @Override
    public PostFormBuilder params(final Map<String, String> params) {
        if (this.params != null && params != null) {
            for (String oldKey : this.params.keySet()) {
                if (!params.containsKey(oldKey)) {
                    params.put(oldKey, this.params.get(oldKey));
                }
            }
        }
        this.params = params;
        return this;
    }

    public static class FileInput {
        public File file;
        public String filename;
        public String key;

        public FileInput(final String key, final String filename, final File file) {
            this.key = key;
            this.filename = filename;
            this.file = file;
        }

        @Override
        public String toString() {
            return "FileInput{key='" + this.key + '\'' + ", filename='" + this.filename + '\'' + ", file=" + this.file + '}';
        }
    }
}
