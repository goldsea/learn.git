package com.nhtzj.common.okhttp;

import android.text.TextUtils;

import com.nhtzj.common.okhttp.builder.GetBuilder;
import com.nhtzj.common.okhttp.builder.HeadBuilder;
import com.nhtzj.common.okhttp.builder.OtherRequestBuilder;
import com.nhtzj.common.okhttp.builder.PostFileBuilder;
import com.nhtzj.common.okhttp.builder.PostFormBuilder;
import com.nhtzj.common.okhttp.builder.PostStringBuilder;
import com.nhtzj.common.okhttp.callback.Callback;
import com.nhtzj.common.okhttp.request.RequestCall;
import com.nhtzj.common.okhttp.utils.Platform;

import java.io.IOException;
import java.util.concurrent.Executor;

import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Response;

/**
 * okhttp请求包装类
 */
public class OkHttpUtils3 {
    public static final String TAG = "OkHttpUtils3";
    public static final long DEFAULT_MILLISECONDS = 10000;
    private static volatile OkHttpUtils3 mInstance;
    private OkHttpClient mOkHttpClient;
    private Platform mPlatform;

    private OkHttpUtils3(OkHttpClient mOkHttpClient) {
        if (mOkHttpClient == null) {
            this.mOkHttpClient = new OkHttpClient();
        } else {
            this.mOkHttpClient = mOkHttpClient;
        }
        this.mPlatform = Platform.get();
    }

    private boolean debug;
    private String tag;

    public OkHttpUtils3 debug(String tag) {
        debug = true;
        this.tag = tag;
        return this;
    }


    public static OkHttpUtils3 getInstance() {
        return initClient(null);
    }

    public static OkHttpUtils3 initClient(OkHttpClient okHttpClient) {
        if (mInstance != null) {
            return mInstance;
        }
        synchronized (OkHttpUtils3.class) {
            if (OkHttpUtils3.mInstance == null) {
                OkHttpUtils3.mInstance = new OkHttpUtils3(okHttpClient);
            }
            return OkHttpUtils3.mInstance;
        }
    }


    public OkHttpClient getOkHttpClient() {
        return mOkHttpClient;
    }


    public Executor getDelivery() {
        return mPlatform.defaultCallbackExecutor();
    }


    public static OtherRequestBuilder delete() {
        return new OtherRequestBuilder("DELETE");
    }

    public static HeadBuilder head() {
        return new HeadBuilder();
    }

    public static GetBuilder get() {
        return new GetBuilder();
    }

    public static OtherRequestBuilder patch() {
        return new OtherRequestBuilder("PATCH");
    }

    public static PostFormBuilder post() {
        return new PostFormBuilder();
    }

    public static PostFileBuilder postFile() {
        return new PostFileBuilder();
    }

    public static PostStringBuilder postString() {
        return new PostStringBuilder();
    }

    public static OtherRequestBuilder put() {
        return new OtherRequestBuilder("PUT");
    }


    public void execute(final RequestCall requestCall, Callback callback_default) {
        if (debug) {
            if (TextUtils.isEmpty(tag)) {
                tag = TAG;
            }
//            XLog.e(tag, "{method:" + requestCall.getRequest().method() + ", detail:" + requestCall.getOkHttpRequest()
//                    .toString() + "}");
        }

        if (callback_default == null)
            callback_default = Callback.CALLBACK_DEFAULT;
        final Callback finalCallback = callback_default;

        requestCall.getCall().enqueue(new okhttp3.Callback() {
            final int id = requestCall.getOkHttpRequest().getId();

            @Override
            public void onFailure(Call call, IOException e) {
                sendFailResultCallback(call, e, finalCallback, id, -1);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.code() >= 400 && response.code() <= 599) {
                    try {
                        sendFailResultCallback(call, new RuntimeException(response.body().string()), finalCallback, id, response.code());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return;
                }

                try {
                    Object o = finalCallback.parseNetworkResponse(response, id);
                    sendSuccessResultCallback(o, finalCallback, id, response.code());
                } catch (Exception e) {
                    sendFailResultCallback(call, e, finalCallback, id, response.code());
                }
            }


        });
    }


    public void sendFailResultCallback(final Call call, final Exception e, final Callback callback, final int id, final int code) {
        if (callback == null) return;

        mPlatform.execute(new Runnable() {
            @Override
            public void run() {
                callback.onError(call, e, id, code);
                callback.onAfter(id);
            }
        });
    }

    public void sendSuccessResultCallback(final Object object, final Callback callback, final int id, final int code) {
        if (callback == null) return;
        mPlatform.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    callback.onResponse(object, id, code);
                } catch (Exception e) {
                    callback.onProcessError(object, e, id);
                }
                callback.onAfter(id);
            }
        });
    }


    public void cancelTag(Object tag) {
        for (Call call : this.mOkHttpClient.dispatcher().queuedCalls()) {
            if (tag.equals(call.request().tag())) {
                call.cancel();
            }
        }
        for (Call call2 : this.mOkHttpClient.dispatcher().runningCalls()) {
            if (tag.equals(call2.request().tag())) {
                call2.cancel();
            }
        }
    }

    public static class METHOD {
        public static final String DELETE = "DELETE";
        public static final String HEAD = "HEAD";
        public static final String PATCH = "PATCH";
        public static final String PUT = "PUT";
    }


}

