package com.nhtzj.common.okhttp.utils;

/**
 * <pre>
 *     author : Haitao
 *     e-mail : xxx@xx
 *     time   : 2017/05/15
 *     desc   :
 *     version: 1.0
 * </pre>
 */


import android.os.Build;
import android.os.Handler;
import android.os.Looper;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;


public class Platform {
    private static final Platform PLATFORM = findPlatform();

    private static Platform findPlatform() {
        try {
            Class.forName("android.os.Build");
            if (Build.VERSION.SDK_INT != 0) {
                Android localAndroid = new Android();
                return localAndroid;
            }
        } catch (ClassNotFoundException localClassNotFoundException) {
        }
        return new Platform();
    }

    public static Platform get() {
        L.e(PLATFORM.getClass().toString());
        return PLATFORM;
    }

    public Executor defaultCallbackExecutor() {
        return Executors.newCachedThreadPool();
    }

    public void execute(Runnable paramRunnable) {
        defaultCallbackExecutor().execute(paramRunnable);
    }

    static class Android extends Platform {
        public Executor defaultCallbackExecutor() {
            return new MainThreadExecutor();
        }

        static class MainThreadExecutor
                implements Executor {
            private final Handler handler = new Handler(Looper.getMainLooper());

            public void execute(Runnable paramRunnable) {
                this.handler.post(paramRunnable);
            }
        }
    }
}