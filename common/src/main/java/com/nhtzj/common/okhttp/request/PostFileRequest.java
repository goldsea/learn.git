package com.nhtzj.common.okhttp.request;


import com.nhtzj.common.okhttp.OkHttpUtils3;
import com.nhtzj.common.okhttp.callback.Callback;
import com.nhtzj.common.okhttp.utils.Exceptions;

import java.io.File;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;

public class PostFileRequest extends OkHttpRequest {
    private static MediaType MEDIA_TYPE_STREAM;

    private File file;
    private MediaType mediaType;

    static {
        PostFileRequest.MEDIA_TYPE_STREAM = MediaType.parse("application/octet-stream");
    }

    public PostFileRequest(String url, Object tag, Map<String, String> params, Map<String, String> headers, File file, MediaType mediaType, int id) {
        super(url, tag, params, headers, id);
        this.file = file;
        this.mediaType = mediaType;
        if (this.file == null) {
            Exceptions.illegalArgument("the file can not be null !", new Object[0]);
        }
        if (this.mediaType == null) {
            this.mediaType = PostFileRequest.MEDIA_TYPE_STREAM;
        }
    }

    @Override
    protected Request buildRequest(final RequestBody requestBody) {
        return this.builder.post(requestBody).build();
    }

    @Override
    protected RequestBody buildRequestBody() {
        return RequestBody.create(this.mediaType, this.file);
    }

    @Override
    protected RequestBody wrapRequestBody(final RequestBody requestBody, final Callback callback) {
        if (callback == null) {
            return requestBody;
        }
        return new CountingRequestBody(requestBody, (CountingRequestBody.Listener) new CountingRequestBody.Listener() {
            @Override
            public void onRequestProgress(final long n, final long n2) {
                OkHttpUtils3.getInstance().getDelivery().execute(new Runnable() {
                    @Override
                    public void run() {
                        callback.inProgress(1.0f * n / n2, n2, PostFileRequest.this.id);
                    }
                });
            }
        });
    }
}

