package com.nhtzj.common.okhttp.builder;


import com.nhtzj.common.okhttp.request.PostFileRequest;
import com.nhtzj.common.okhttp.request.RequestCall;

import java.io.File;

import okhttp3.MediaType;

public class PostFileBuilder extends OkHttpRequestBuilder<PostFileBuilder> {
    private File file;
    private MediaType mediaType;

    @Override
    public RequestCall build() {
        return new PostFileRequest(this.url, this.tag, this.params, this.headers, this.file, this.mediaType, this.id).build();
    }

    public PostFileBuilder file(final File file) {
        this.file = file;
        return this;
    }

    public PostFileBuilder mediaType(final MediaType mediaType) {
        this.mediaType = mediaType;
        return this;
    }
}