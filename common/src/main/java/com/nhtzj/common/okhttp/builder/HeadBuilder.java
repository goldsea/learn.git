package com.nhtzj.common.okhttp.builder;

import com.nhtzj.common.okhttp.request.OtherRequest;
import com.nhtzj.common.okhttp.request.RequestCall;

/**
 * <pre>
 *     author : Haitao
 *     e-mail : xxx@xx
 *     time   : 2017/05/15
 *     desc   :
 *     version: 1.0
 * </pre>
 */


public class HeadBuilder extends com.nhtzj.common.okhttp.builder.GetBuilder {
    @Override
    public RequestCall build() {
        return new OtherRequest(null, null, "HEAD", this.url, this.tag, this.params, this.headers, this.id).build();
    }
}
