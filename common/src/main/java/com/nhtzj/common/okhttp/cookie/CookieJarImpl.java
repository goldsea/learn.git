package com.nhtzj.common.okhttp.cookie;

import com.nhtzj.common.okhttp.cookie.store.CookieStore;
import com.nhtzj.common.okhttp.utils.Exceptions;

import java.util.List;

import okhttp3.Cookie;
import okhttp3.CookieJar;
import okhttp3.HttpUrl;


/**
 * <pre>
 *     author : Haitao
 *     e-mail : xxx@xx
 *     time   : 2017/05/15
 *     desc   :
 *     version: 1.0
 * </pre>
 */


public class CookieJarImpl implements CookieJar {
    private CookieStore cookieStore;

    public CookieJarImpl(final CookieStore cookieStore) {
        if (cookieStore == null) {
            Exceptions.illegalArgument("cookieStore can not be null.", new Object[0]);
        }
        this.cookieStore = cookieStore;
    }

    public CookieStore getCookieStore() {
        return this.cookieStore;
    }

    public List<Cookie> loadForRequest(final HttpUrl httpUrl) {
        synchronized (this) {
            return this.cookieStore.get(httpUrl);
        }
    }

    public void saveFromResponse(final HttpUrl httpUrl, final List<Cookie> list) {
        synchronized (this) {
            this.cookieStore.add(httpUrl, list);
        }
    }
}
