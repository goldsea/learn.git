package com.nhtzj.common.okhttp.cookie.store;

/**
 * <pre>
 *     author : Haitao
 *     e-mail : xxx@xx
 *     time   : 2017/05/15
 *     desc   :
 *     version: 1.0
 * </pre>
 */


public interface HasCookieStore {
    CookieStore getCookieStore();
}
