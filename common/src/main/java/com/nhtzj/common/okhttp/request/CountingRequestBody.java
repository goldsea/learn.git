package com.nhtzj.common.okhttp.request;


import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okio.Buffer;
import okio.BufferedSink;
import okio.ForwardingSink;
import okio.Okio;
import okio.Sink;

/**
 * Decorates an OkHttp request body to count the number of bytes written when writing it. Can
 * decorate any request body, but is most useful for tracking the upload progress of large
 * multipart requests.
 *
 * @author Leo Nikkilä
 */

public class CountingRequestBody extends RequestBody {
    protected CountingSink countingSink;
    protected RequestBody delegate;
    protected Listener listener;

    public CountingRequestBody(RequestBody delegate, Listener listener) {
        this.delegate = delegate;
        this.listener = listener;
    }

    public long contentLength() {
        try {
            return this.delegate.contentLength();
        } catch (IOException ex) {
            ex.printStackTrace();
            return -1L;
        }
    }

    public MediaType contentType() {
        return this.delegate.contentType();
    }

    public void writeTo(BufferedSink bufferedSink) throws IOException {
        this.countingSink = new CountingSink((Sink) bufferedSink);
        BufferedSink buffer = Okio.buffer((Sink) this.countingSink);
        this.delegate.writeTo(buffer);
        buffer.flush();
    }

    protected final class CountingSink extends ForwardingSink {
        private long bytesWritten;

        public CountingSink(Sink sink) {
            super(sink);
            this.bytesWritten = 0L;
        }

        public void write(Buffer buffer, long n) throws IOException {
            super.write(buffer, n);
            this.bytesWritten += n;
            CountingRequestBody.this.listener.onRequestProgress(this.bytesWritten, CountingRequestBody.this.contentLength());
        }
    }

    public interface Listener {
        void onRequestProgress(long p0, long p1);
    }
}
