package com.nhtzj.common.okhttp.builder;


import com.nhtzj.common.okhttp.request.RequestCall;

import java.util.LinkedHashMap;
import java.util.Map;

public abstract class OkHttpRequestBuilder<T extends OkHttpRequestBuilder> {
    protected Map<String, String> headers;
    protected int id;
    protected Map<String, String> params;
    protected Object tag;
    protected String url;

    public T addHeader(String paramString1, String paramString2) {
        if (this.headers == null) {
            this.headers = new LinkedHashMap();
        }
        this.headers.put(paramString1, paramString2);
        return (T) this;
    }

    public abstract RequestCall build();

    public T headers(Map<String, String> paramMap) {
        this.headers = paramMap;
        return (T) this;
    }

    public T id(int paramInt) {
        this.id = paramInt;
        return (T) this;
    }

    public T tag(Object paramObject) {
        this.tag = paramObject;
        return (T) this;
    }

    public T url(String paramString) {
        this.url = paramString;
        return (T) this;
    }
}