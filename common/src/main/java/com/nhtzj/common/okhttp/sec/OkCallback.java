package com.nhtzj.common.okhttp.sec;

import okhttp3.Call;
import okhttp3.Request;

/**
 * <pre>
 *     author : Haitao
 *     e-mail : xxx@xx
 *     time   : 2017/05/16
 *     desc   : okhttp自定义回调
 *     version: 1.0
 * </pre>
 */


public abstract class OkCallback<T> {

    /**
     * UI Thread
     *  请求前调用
     * @param request
     * @param id
     */
    public void onBefore(Request request, int id) {
    }

    /**
     * UI Thread
     *  请求结束后调用
     * @param id
     * @param
     */
    public void onAfter(int id) {
    }

    /**
     * UI Thread
     *
     * @param progress
     */
    public void inProgress(float progress, long totalLen, int id) {

    }

    /**
     *网络异常
     * @param call
     * @param e
     * @param id
     * @param code
     */
    public abstract void onNetError(Call call, Exception e, int id, int code);

    /**
     *数据处理过程中发生异常
     * @param response
     * @param e
     * @param id
     */
    public abstract void onParseError(T response, Exception e, int id);

    /**
     * 处理数据
     * @param response
     * @param id
     * @param code
     * @throws Exception
     */
    public abstract void onResponse(T response, int id, int code) throws Exception;

}
