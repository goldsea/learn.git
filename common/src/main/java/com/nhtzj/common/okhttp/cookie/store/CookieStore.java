package com.nhtzj.common.okhttp.cookie.store;

import java.util.List;

import okhttp3.Cookie;
import okhttp3.HttpUrl;

/**
 * <pre>
 *     author : Haitao
 *     e-mail : xxx@xx
 *     time   : 2017/05/15
 *     desc   :
 *     version: 1.0
 * </pre>
 */


public interface CookieStore {
    void add(final HttpUrl url, final List<Cookie> cookies);

    List<Cookie> get(final HttpUrl p0);

    List<Cookie> getCookies();

    boolean remove(final HttpUrl p0, final Cookie p1);

    boolean removeAll();
}