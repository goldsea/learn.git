package com.nhtzj.common.okhttp.sec;


import android.os.UserManager;
import android.support.annotation.IntDef;


import com.nhtzj.common.okhttp.OkHttpUtils3;
import com.nhtzj.common.okhttp.builder.GetBuilder;
import com.nhtzj.common.okhttp.builder.OkHttpRequestBuilder;
import com.nhtzj.common.okhttp.builder.PostFormBuilder;
import com.nhtzj.common.okhttp.callback.Callback;
import com.nhtzj.common.okhttp.callback.JsonCallback;
import com.nhtzj.common.okhttp.callback.StringCallback;
import com.nhtzj.common.util.XLog;

import org.json.JSONObject;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Request;

/**
 * <pre>
 *     author : Haitao
 *     e-mail : haitao_ni@foxmail.com
 *     time   : 2017/05/16
 *     desc   : okhttp二次封装
 *     version: 1.0
 * </pre>
 */


public class OHS3 {

    public static final int POST = 1;
    public static final int GET = 2;

    public static final int JSON = 1;
    public static final int STRING = 2;
    public static final int SIMPLESTRING = 3;

    @IntDef({GET, POST})
    @Retention(RetentionPolicy.SOURCE)
    public @interface ReqyestType {
    }

    @IntDef({JSON, STRING, SIMPLESTRING})
    @Retention(RetentionPolicy.SOURCE)
    public @interface CallbackType {
    }

    /**
     * @param tag
     * @param url      请求地址
     * @param callback 回调 {@link OkJoCallback}
     */
    public static void getJoBack(Object tag, String url, OkCallback callback) {
        sendRequest(tag, GET, JSON, url, false, null, -1, callback);
    }

    /**
     * @param tag
     * @param url      请求地址
     * @param params   参数
     * @param callback 回调 {@link OkJoCallback}
     */
    public static void getJoBack(Object tag, String url, Map<String, String> params, OkCallback callback) {
        sendRequest(tag, GET, JSON, url, false, params, -1, callback);
    }

    public static void getJoBackAuth(Object tag, String url, OkCallback callback) {
        sendRequest(tag, GET, JSON, url, true, null, -1, callback);
    }

    public static void getJoBackAuth(Object tag, String url, Map<String, String> params, OkCallback callback) {
        sendRequest(tag, GET, JSON, url, true, params, -1, callback);
    }

    public static void getStrBack(Object tag, String url, OkCallback callback) {
        sendRequest(tag, GET, STRING, url, false, null, -1, callback);
    }

    public static void getStrBackAuth(Object tag, String url, OkCallback callback) {
        sendRequest(tag, GET, STRING, url, true, null, -1, callback);
    }

    public static void getStrBackAuth(Object tag, String url, Map<String, String> params, OkCallback callback) {
        sendRequest(tag, GET, STRING, url, true, params, -1, callback);
    }

    public static void postJoBack(Object tag, String url, OkCallback callback) {
        sendRequest(tag, POST, JSON, url, false, null, -1, callback);
    }


    /**
     * post方式请求数据
     *
     * @param tag
     * @param url      请求地址
     * @param params   参数
     * @param callback 回调 {@link OkJoCallback}
     */
    public static void postJoBack(Object tag, String url, Map<String, String> params, OkCallback callback) {
        sendRequest(tag, POST, JSON, url, false, params, -1, callback);
    }

    public static void postJoBackAuth(Object tag, String url, OkCallback callback) {
        sendRequest(tag, POST, JSON, url, true, null, -1, callback);
    }

    /**
     * post方式请求数据,返回JsonObject 需要登录token
     *
     * @param tag
     * @param url      请求地址
     * @param params   参数
     * @param callback 回调 {@link OkJoCallback}
     */
    public static void postJoBackAuth(Object tag, String url, Map<String, String> params, OkCallback callback) {
        sendRequest(tag, POST, JSON, url, true, params, -1, callback);
    }


    public static void postStrBack(Object tag, String url, OkCallback callback) {
        sendRequest(tag, POST, STRING, url, false, null, -1, callback);
    }

    public static void postStrBack(Object tag, String url, Map<String, String> params, OkCallback callback) {
        sendRequest(tag, POST, STRING, url, false, params, -1, callback);
    }

    /**
     * @param tag
     * @param url      请求地址
     * @param params   参数
     * @param id       标志
     * @param callback 回调 {@link OkStrCallback}
     */
    public static void postStrBack(Object tag, String url, Map<String, String> params, int id, OkCallback callback) {
        sendRequest(tag, POST, STRING, url, false, params, id, callback);
    }

    public static void postStrBackAuth(Object tag, String url, OkCallback callback) {
        sendRequest(tag, POST, STRING, url, true, null, -1, callback);
    }

    /**
     * post方式请求数据,返回String 需要登录token
     *
     * @param tag
     * @param url      请求地址
     * @param params   参数
     * @param id       标志
     * @param callback 回调 {@link OkStrCallback}
     */
    public static void postStrBackAuth(Object tag, String url, Map<String, String> params, int id, OkCallback callback) {
        sendRequest(tag, POST, STRING, url, true, params, id, callback);
    }


    /**
     * post方式请求数据,返回String 需要登录token
     *
     * @param tag
     * @param url      请求地址
     * @param params   参数
     * @param callback 回调 {@link OkStrCallback}
     */
    public static void postStrBackAuth(Object tag, String url, Map<String, String> params, OkCallback callback) {
        sendRequest(tag, POST, STRING, url, true, params, -1, callback);
    }

    /**
     * @param tag
     * @param url      请求地址
     * @param auth     是否需要token
     *                 <ul>
     *                 <li>true 需要</li>
     *                 <li>false 不需要</li>
     * @param params   参数
     * @param id       标志
     * @param callback 回调 {@link OkStrCallback}
     */
    public static void postStrBack(Object tag, String url, boolean auth, Map<String, String> params, int id, final OkCallback callback) {
        sendRequest(tag, POST, STRING, url, auth, params, id, callback);
    }

    /**
     * 请求基方法
     *
     * @param tag
     * @param requestType  请求方式
     *                     <ul>
     *                     <li>OHS3.POST</li>
     *                     <li>OHS3.GET</li>
     *                     </ui>
     * @param callbackType 回调方式
     *                     <ul>
     *                     <li>OHS3.JSON</li>
     *                     <li>OHS3.STRING</li>
     *                     </ui>
     * @param url          请求地址
     * @param auth         是否需要token
     *                     <ul>
     *                     <li>true 需要</li>
     *                     <li>false 不需要</li>
     *                     </ui>
     * @param params       请求参数
     * @param id           标志
     * @param callback     回调
     *                     <ul>
     *                     <li>{@link OkStrCallback}-返回String</li>
     *                     <li>{@link OkJoCallback}-返回OkJoCallback</li>
     *                     </ui>
     */
    public static void sendRequest(Object tag, @ReqyestType int requestType, @CallbackType int callbackType, String url, boolean auth, Map<String, String> params, int id, OkCallback callback) {
        OkHttpRequestBuilder okHttpRequestBuilder = generateRequestBuilder(tag, requestType, url, auth, params, id);
        Callback<Object> objectCallback = generateCallback(callbackType, callback);
        okHttpRequestBuilder.build()
                .execute(objectCallback);

    }

    private static OkHttpRequestBuilder generateRequestBuilder(Object tag, @ReqyestType int requestType, String url, boolean auth, Map<String, String> params, int id) {
        OkHttpRequestBuilder requestBuilder = null;
        switch (requestType) {
            case POST:
                requestBuilder = generatePostBuilder(tag, url, auth, params, id);
                break;
            case GET:
                requestBuilder = generateGetBuilder(tag, url, auth, params, id);
                break;
        }


        return requestBuilder;
    }

    private static PostFormBuilder generatePostBuilder(Object tag, String url, boolean auth, Map<String, String> params, int id) {
        PostFormBuilder postFormBuilder = OkHttpUtils3.post().url(url)
                .params(params)
                .id(id)
                .tag(tag)
                .addHeader("Accept", "application/json");

        if (auth) {
//            postFormBuilder.addHeader("Authorization", "Bearer " + UserManager.getUserManager().getAccessToken());
        }

        return postFormBuilder;
    }

    private static GetBuilder generateGetBuilder(Object tag, String url, boolean auth, Map<String, String> params, int id) {
        GetBuilder getBuilder = OkHttpUtils3.get().url(url)
                .params(params)
                .id(id)
                .tag(tag);
        if (auth) {
//            getBuilder.addHeader("Authorization", "Bearer " + UserManager.getUserManager().getAccessToken());
        }

        return getBuilder;
    }

    private static <T> Callback<T> generateCallback(@CallbackType int type, OkCallback okCallback) {
        Callback callback = null;
        switch (type) {
            case JSON:
                callback = generateJsonCallback(okCallback);
                break;
            case STRING:
                callback = generateStrCallback(okCallback);
                break;
        }

        return callback;
    }


    private static Callback<String> generateStrCallback(OkCallback okCallback) {
        final OkCallback callback = okCallback;
        return new StringCallback() {

            @Override
            public void onBefore(Request request, int id) {
                super.onBefore(request, id);
                if (callback != null) {
                    callback.onBefore(request, id);
                }
            }

            @Override
            public void onError(Call call, Exception e, int id, int code) {
                XLog.e(e);
                if (callback != null) {
                    callback.onNetError(call, e, id, code);
                }
            }

            @Override
            public void onProcessError(String response, Exception e, int id) {
                if (callback != null) {
                    callback.onParseError(response, e, id);
                }
            }

            @Override
            public void onResponse(String response, int id, int code) throws Exception {
                if (callback != null) {
                    callback.onResponse(response, id, code);
                }
            }

            @Override
            public void onAfter(int id) {
                super.onAfter(id);
                if (callback != null) {
                    callback.onAfter(id);
                }
            }

        };
    }

    private static Callback<JSONObject> generateJsonCallback(OkCallback okCallback) {
        final OkCallback callback = okCallback;
        return new JsonCallback() {

            @Override
            public void onBefore(Request request, int id) {
                super.onBefore(request, id);
                if (callback != null) {
                    callback.onBefore(request, id);
                }
            }

            @Override
            public void onError(Call call, Exception e, int id, int code) {
                XLog.e(e);
                if (callback != null) {
                    callback.onNetError(call, e, id, code);
                }
            }

            @Override
            public void onProcessError(JSONObject response, Exception e, int id) {
                if (callback != null) {
                    callback.onParseError(response, e, id);
                }
            }

            @Override
            public void onResponse(JSONObject response, int id, int code) throws Exception {
//                XLog.e(response.toString());
                if (callback != null) {
                    callback.onResponse(response, id, code);
                }
            }

            @Override
            public void onAfter(int id) {
                super.onAfter(id);
                if (callback != null) {
                    callback.onAfter(id);
                }
            }

        };
    }

    public static void cancelTag(Object tag) {
        OkHttpUtils3.getInstance().cancelTag(tag);
    }

}
