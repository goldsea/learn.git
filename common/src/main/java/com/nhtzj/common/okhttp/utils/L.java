package com.nhtzj.common.okhttp.utils;

import android.util.Log;

/**
 * <pre>
 *     author : Haitao
 *     e-mail : xxx@xx
 *     time   : 2017/05/15
 *     desc   :
 *     version: 1.0
 * </pre>
 */


public class L {
    private static boolean debug = false;

    public static void e(String paramString) {
        if (debug)
            Log.e("OkHttp", paramString);
    }
}