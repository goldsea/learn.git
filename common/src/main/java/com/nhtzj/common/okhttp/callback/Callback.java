package com.nhtzj.common.okhttp.callback;


import okhttp3.Call;
import okhttp3.Request;
import okhttp3.Response;

public abstract class Callback<T> {
    /**
     * UI Thread
     *
     * @param request
     */
    public void onBefore(Request request, int id) {
    }

    /**
     * UI Thread
     *
     * @param
     */
    public void onAfter(int id) {
    }

    /**
     * UI Thread
     *
     * @param progress
     */
    public void inProgress(float progress, long n2, int id) {

    }

    /**
     * Thread Pool Thread
     *
     * @param response
     */
    public abstract T parseNetworkResponse(Response response, int id) throws Exception;

    public abstract void onError(Call call, Exception e, int id,int code);

    public abstract void onProcessError(T response, Exception e, int id);

    public abstract void onResponse(T response, int id,int code) throws Exception;

    public boolean validateReponse(Response paramResponse, int paramInt) {
        return paramResponse.isSuccessful();
    }


    public static Callback CALLBACK_DEFAULT = new Callback() {

        @Override
        public Object parseNetworkResponse(Response response, int id) throws Exception {
            return null;
        }

        @Override
        public void onError(Call call, Exception e, int id,int code) {

        }

        @Override
        public void onProcessError(Object response, Exception e, int id) {

        }

        @Override
        public void onResponse(Object response, int id,int code) {

        }
    };

}