package com.nhtzj.common.okhttp.builder;


import android.net.Uri;

import com.nhtzj.common.okhttp.request.GetRequest;
import com.nhtzj.common.okhttp.request.RequestCall;

import java.util.LinkedHashMap;
import java.util.Map;

public class GetBuilder extends com.nhtzj.common.okhttp.builder.OkHttpRequestBuilder<GetBuilder> implements com.nhtzj.common.okhttp.builder.HasParamsable {

    @Override
    public GetBuilder addParams(String paramString1, String paramString2) {
        if (this.params == null) {
            this.params = new LinkedHashMap<String, String>();
        }
        this.params.put(paramString1, paramString2);
        return this;
    }

    protected String appendParams(final String s, final Map<String, String> map) {
        if (s == null || map == null || map.isEmpty()) {
            return s;
        }
        final Uri.Builder buildUpon = Uri.parse(s).buildUpon();
        for (final String s2 : map.keySet()) {
            buildUpon.appendQueryParameter(s2, (String) map.get(s2));
        }
        return buildUpon.build().toString();
    }

    @Override
    public GetBuilder params(Map<String, String> params) {
        if (this.params != null && params != null) {
            for (String oldKey : this.params.keySet()) {
                if (!params.containsKey(oldKey)) {
                    params.put(oldKey, this.params.get(oldKey));
                }
            }
        }
        this.params = params;
        return this;
    }

    @Override
    public RequestCall build() {
        if (this.params != null) {
            this.url = this.appendParams(this.url, params);
        }
        return new GetRequest(url, tag, params, headers, id).build();
    }
}
