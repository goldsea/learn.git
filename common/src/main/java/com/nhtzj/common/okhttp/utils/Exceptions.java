package com.nhtzj.common.okhttp.utils;

public class Exceptions {
    public static void illegalArgument(final String s, final Object... array) {
        throw new IllegalArgumentException(String.format(s, array));
    }
}
