package com.nhtzj.common.okhttp.cookie.store;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Log;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import okhttp3.Cookie;
import okhttp3.HttpUrl;

/**
 * <pre>
 *     author : Haitao
 *     e-mail : xxx@xx
 *     time   : 2017/05/15
 *     desc   :
 *     version: 1.0
 * </pre>
 */


public class PersistentCookieStore implements CookieStore {
    private static final String COOKIE_NAME_PREFIX = "cookie_";
    private static final String COOKIE_PREFS = "CookiePrefsFile";
    private static final String LOG_TAG = "PersistentCookieStore";
    private final SharedPreferences cookiePrefs;
    private final HashMap<String, ConcurrentHashMap<String, Cookie>> cookies;

    public PersistentCookieStore(Context paramContext) {
        cookiePrefs = paramContext.getSharedPreferences("CookiePrefsFile", 0);
        cookies = new HashMap();
        Iterator localIterator = cookiePrefs.getAll().entrySet().iterator();
        while (localIterator.hasNext()) {
            Map.Entry localEntry = (Map.Entry) localIterator.next();
            if (((String) localEntry.getValue() != null) && (!((String) localEntry.getValue()).startsWith("cookie_")))
                for (String str1 : TextUtils.split((String) localEntry.getValue(), ",")) {
                    String str2 = cookiePrefs.getString("cookie_" + str1, null);
                    if (str2 != null) {
                        Cookie localCookie = decodeCookie(str2);
                        if (localCookie != null) {
                            if (!cookies.containsKey(localEntry.getKey()))
                                cookies.put(String.valueOf(localEntry.getKey()), new ConcurrentHashMap());
                            ((ConcurrentHashMap) cookies.get(localEntry.getKey())).put(str1, localCookie);
                        }
                    }
                }
        }
    }

    private static boolean isCookieExpired(Cookie paramCookie) {
        return paramCookie.expiresAt() < System.currentTimeMillis();
    }

    public void add(HttpUrl paramHttpUrl, List<Cookie> paramList) {
        Iterator localIterator = paramList.iterator();
        while (localIterator.hasNext())
            add(paramHttpUrl, (Cookie) localIterator.next());
    }

    protected void add(HttpUrl httpUrl, Cookie cookie) {
        String cookieToken = getCookieToken(cookie);
        if (cookie.persistent()) {
            if (!cookies.containsKey(httpUrl.host())) {
                cookies.put(httpUrl.host(), new ConcurrentHashMap<String, Cookie>());
            }
            cookies.get(httpUrl.host()).put(cookieToken, cookie);
        } else {
            if (!cookies.containsKey(httpUrl.host())) {
                return;
            }
            cookies.get(httpUrl.host()).remove(cookieToken);
        }
        final SharedPreferences.Editor edit = cookiePrefs.edit();
        edit.putString(httpUrl.host(), TextUtils.join((CharSequence) ",", (Iterable) cookies.get(httpUrl.host()).keySet()));
        edit.putString("cookie_" + cookieToken, encodeCookie(new SerializableHttpCookie(cookie)));
        edit.apply();
    }

    protected String byteArrayToHexString(byte[] paramArrayOfByte) {
        StringBuilder localStringBuilder = new StringBuilder(2 * paramArrayOfByte.length);
        int i = paramArrayOfByte.length;
        for (int j = 0; j < i; j++) {
            int k = 0xFF & paramArrayOfByte[j];
            if (k < 16)
                localStringBuilder.append('0');
            localStringBuilder.append(Integer.toHexString(k));
        }
        return localStringBuilder.toString().toUpperCase(Locale.US);
    }

    protected Cookie decodeCookie(String paramString) {
        ByteArrayInputStream localByteArrayInputStream = new ByteArrayInputStream(hexStringToByteArray(paramString));
        try {
            Cookie localCookie = ((SerializableHttpCookie) new ObjectInputStream(localByteArrayInputStream).readObject()).getCookie();
            return localCookie;
        } catch (IOException localIOException) {
            Log.d("PersistentCookieStore", "IOException in decodeCookie", localIOException);
            return null;
        } catch (ClassNotFoundException localClassNotFoundException) {
            Log.d("PersistentCookieStore", "ClassNotFoundException in decodeCookie", localClassNotFoundException);
        }
        return null;
    }

    protected String encodeCookie(SerializableHttpCookie paramSerializableHttpCookie) {
        if (paramSerializableHttpCookie == null)
            return null;
        ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
        try {
            new ObjectOutputStream(localByteArrayOutputStream).writeObject(paramSerializableHttpCookie);
            return byteArrayToHexString(localByteArrayOutputStream.toByteArray());
        } catch (IOException localIOException) {
            Log.d("PersistentCookieStore", "IOException in encodeCookie", localIOException);
        }
        return null;
    }

    public List<Cookie> get(HttpUrl paramHttpUrl) {
        ArrayList localArrayList = new ArrayList();
        if (cookies.containsKey(paramHttpUrl.host())) {
            Iterator localIterator = ((ConcurrentHashMap) cookies.get(paramHttpUrl.host())).values().iterator();
            while (localIterator.hasNext()) {
                Cookie localCookie = (Cookie) localIterator.next();
                if (isCookieExpired(localCookie))
                    remove(paramHttpUrl, localCookie);
                else
                    localArrayList.add(localCookie);
            }
        }
        return localArrayList;
    }

    protected String getCookieToken(Cookie paramCookie) {
        return paramCookie.name() + paramCookie.domain();
    }

    public List<Cookie> getCookies() {
        ArrayList localArrayList = new ArrayList();
        Iterator localIterator = cookies.keySet().iterator();
        while (localIterator.hasNext()) {
            String str = (String) localIterator.next();
            localArrayList.addAll(((ConcurrentHashMap) cookies.get(str)).values());
        }
        return localArrayList;
    }

    protected byte[] hexStringToByteArray(String paramString) {
        int i = paramString.length();
        byte[] arrayOfByte = new byte[i / 2];
        for (int j = 0; j < i; j += 2)
            arrayOfByte[(j / 2)] = ((byte) ((Character.digit(paramString.charAt(j), 16) << 4) + Character.digit(paramString.charAt(j + 1), 16)));
        return arrayOfByte;
    }

    public boolean remove(HttpUrl paramHttpUrl, Cookie paramCookie) {
        String str = getCookieToken(paramCookie);
        if ((cookies.containsKey(paramHttpUrl.host())) && (((ConcurrentHashMap) cookies.get(paramHttpUrl.host())).containsKey(str))) {
            ((ConcurrentHashMap) cookies.get(paramHttpUrl.host())).remove(str);
            SharedPreferences.Editor localEditor = cookiePrefs.edit();
            if (cookiePrefs.contains("cookie_" + str))
                localEditor.remove("cookie_" + str);
            localEditor.putString(paramHttpUrl.host(), TextUtils.join(",", ((ConcurrentHashMap) cookies.get(paramHttpUrl.host())).keySet()));
            localEditor.apply();
            return true;
        }
        return false;
    }

    public boolean removeAll() {
        SharedPreferences.Editor localEditor = cookiePrefs.edit();
        localEditor.clear();
        localEditor.apply();
        cookies.clear();
        return true;
    }
}