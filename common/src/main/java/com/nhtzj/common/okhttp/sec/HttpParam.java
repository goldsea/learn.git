package com.nhtzj.common.okhttp.sec;

import java.util.HashMap;

/**
 * <pre>
 * author : Haitao
 * e-mail : haitao_ni@foxmail.com
 * time   : 2017/08/11
 * desc   :
 * version: 1.0
 * </pre>
 */


public class HttpParam extends HashMap<String, Object>
{
    @Override
    public HttpParam put(final String s, final Object o) {
        super.put(s, o);
        return this;
    }
}
