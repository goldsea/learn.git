package com.nhtzj.common.okhttp.builder;

import java.util.Map;

/**
 * <pre>
 *     author : Haitao
 *     e-mail : xxx@xx
 *     time   : 2017/05/15
 *     desc   :
 *     version: 1.0
 * </pre>
 */


public interface HasParamsable {
    public abstract com.nhtzj.common.okhttp.builder.OkHttpRequestBuilder addParams(String paramString1, String paramString2);

    public abstract com.nhtzj.common.okhttp.builder.OkHttpRequestBuilder params(Map<String, String> paramMap);
}
