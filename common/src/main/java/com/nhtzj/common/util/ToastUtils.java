package com.nhtzj.common.util;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.view.Gravity;
import android.widget.Toast;

/**
 * <pre>
 *     author : Haitao
 *     e-mail : xxx@xx
 *     time   : 2017/05/16
 *     desc   :
 *     version: 1.0
 * </pre>
 */


public class ToastUtils {

    private static Toast mToast;

    static {
        ToastUtils.mToast = null;
    }

    public static void destory() {
        if (ToastUtils.mToast != null) {
            ToastUtils.mToast = null;
        }
    }

    public static void makeText(Activity activity, final String msg) {
        makeText(activity, msg, Gravity.BOTTOM);
    }

    public static void makeText(Activity activity, final String msg, final int gravity) {
        if (activity == null) {
            return;
        }
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (ToastUtils.mToast == null) {
                    ToastUtils.mToast = Toast.makeText(ContextUtils.getContext(), (CharSequence) msg, Toast.LENGTH_SHORT);
                } else {
                    ToastUtils.mToast.setText(msg);
                    if (msg.length() < 20) {
                        ToastUtils.mToast.setDuration(Toast.LENGTH_SHORT);
                    } else {
                        ToastUtils.mToast.setDuration(Toast.LENGTH_LONG);
                    }
                }
                ToastUtils.mToast.setGravity(gravity, 0, 0);
                ToastUtils.mToast.show();
//                OkToastUtils.mToast = null;
            }
        });
    }

    @Deprecated
    public static void makeText(Context context, CharSequence msg) {
        makeText(context, msg, Gravity.BOTTOM);
    }

    public static void makeText(Context context, CharSequence msg, int gravity) {
        if (msg == null) {
            return;
        }
        if (ToastUtils.mToast == null) {
            ToastUtils.mToast = Toast.makeText(context, msg, Toast.LENGTH_SHORT);
        } else {
            ToastUtils.mToast.setText(msg);
            ToastUtils.mToast.setDuration(Toast.LENGTH_SHORT);
        }
        ToastUtils.mToast.setGravity(gravity, 0, 0);
        ToastUtils.mToast.show();
//        OkToastUtils.mToast = null;
    }

    public static void makeText(CharSequence text) {
        makeText(text, Gravity.BOTTOM);
    }

    public static void makeText(CharSequence text, int gravity) {
        if (text == null) {
            return;
        }
        if (ToastUtils.mToast == null) {
            ToastUtils.mToast = Toast.makeText(ContextUtils.getContext(), text, Toast.LENGTH_SHORT);
        } else {
            ToastUtils.mToast.setText(text);
            ToastUtils.mToast.setDuration(Toast.LENGTH_SHORT);
        }
        show(gravity);
//        OkToastUtils.mToast = null;
    }


    public static void makeText(int msgRes) {
        makeText(msgRes, Gravity.BOTTOM);
    }

    public static void makeText(int msgRes, int gravity) {
        if (msgRes == 0) {
            return;
        }
        if (ToastUtils.mToast == null) {
            ToastUtils.mToast = Toast.makeText(ContextUtils.getContext(), msgRes, Toast.LENGTH_SHORT);
        } else {
            ToastUtils.mToast.setText(msgRes);
            ToastUtils.mToast.setDuration(Toast.LENGTH_SHORT);
        }
        show(gravity);
//        OkToastUtils.mToast = null;
    }

    public static void makeText(Activity activity, int msgRes) {
        makeText(activity, msgRes, Gravity.BOTTOM);
    }

    public static void makeText(final Activity activity, final int messageRes, final int gravity) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                CharSequence message = activity.getResources().getString(messageRes);
                if (TextUtils.isEmpty(message)) {
                    return;
                }
                if (mToast == null) {
                    mToast = Toast.makeText(activity, message, Toast.LENGTH_SHORT);
                } else {
                    mToast.setText(message);
                    if (message.length() < 20)
                        mToast.setDuration(Toast.LENGTH_SHORT);
                    else
                        mToast.setDuration(Toast.LENGTH_LONG);
                }
                show(gravity);
            }
        });
    }

    private static void show(int gravity) {
        ToastUtils.mToast.setGravity(gravity, 0, SizeUtils.dp2px(64));
        ToastUtils.mToast.show();
    }
}

