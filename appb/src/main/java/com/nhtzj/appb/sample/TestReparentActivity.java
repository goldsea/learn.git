package com.nhtzj.appb.sample;

import android.os.Bundle;
import android.widget.TextView;

import com.nhtzj.appb.R;
import com.nhtzj.appb.base.activity.BaseAppCompatActivity;

import butterknife.BindView;

public class TestReparentActivity extends BaseAppCompatActivity {

    @BindView(R.id.tv)
    TextView tv;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_test_reparent;
    }

    @Override
    protected void initView() {
        super.initView();
        tv.setText(activityInstance.toString() + "\nTaskId >>> " + getTaskId());
    }
}
