package com.nhtzj.appb.sample;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.nhtzj.appb.R;
import com.nhtzj.appb.base.activity.BaseAppCompatActivity;

import butterknife.BindView;
import butterknife.OnClick;

public class MainActivity extends BaseAppCompatActivity {

    @BindView(R.id.tv)
    TextView tv;
    @BindView(R.id.btn_standard)
    Button btn;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected void initView() {
        super.initView();
        tv.setText(activityInstance.toString() + "\nTaskId >>> " + getTaskId());
    }

    @OnClick({R.id.btn_standard})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_standard:
                StandardActivity.show(activityInstance);
                break;
        }
    }

}
