package com.nhtzj.appb.sample;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.nhtzj.appb.R;
import com.nhtzj.appb.base.activity.BaseAppCompatActivity;

import butterknife.BindView;

public class StandardActivity extends BaseAppCompatActivity {

    @BindView(R.id.tv)
    TextView tv;

    public static void show(Context context) {
        if (context != null) {
            context.startActivity(new Intent(context, StandardActivity.class));
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_test_reparent;
    }

    @Override
    protected void initView() {
        super.initView();
        tv.setText(activityInstance.toString() + "\nTaskId >>> " + getTaskId());
    }
}
