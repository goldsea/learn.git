package com.nhtzj.appb.base.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.nhtzj.common.util.XLog;

import butterknife.ButterKnife;

/**
 * <pre>
 * author : Haitao
 * blog   : http://blog.nhtzj.com
 * time   : 2018/4/11
 * desc   :
 * version: 2.0
 * </pre>
 */
public abstract class BaseAppCompatActivity extends AppCompatActivity   {
    protected String TAG;
    private boolean mIsDestroy;
    private Fragment mFragment;
    public BaseAppCompatActivity activityInstance;

    private ProgressDialog _waitDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TAG = this.getClass().getSimpleName();

        XLog.e(TAG,"onCreate");
        if (initBundle(getIntent().getExtras())) {
            setContentView(getLayoutId());
            activityInstance = this;
            initWindow();
            initData();
            ButterKnife.bind(this);
            initView();
            requestData();
            bindEvent();
        } else {
            finish();
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        XLog.e(TAG,"onStart");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        XLog.e(TAG,"onRestart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        XLog.e(TAG,"onResume");
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        XLog.e(TAG,"onNewIntent");
    }

    @Override
    protected void onPause() {
        super.onPause();
        XLog.e(TAG,"onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        XLog.e(TAG,"onStop");
    }

    @Override
    protected void onDestroy() {
        XLog.e(TAG,"onDestroy");
        mIsDestroy = true;
        super.onDestroy();
        activityInstance = null;
    }

    protected void addFragment(int frameLayoutId, Fragment fragment) {
        if (fragment != null) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            if (fragment.isAdded()) {
                if (mFragment != null) {
                    transaction.hide(mFragment).show(fragment);
                } else {
                    transaction.show(fragment);
                }
            } else {
                if (mFragment != null) {
                    transaction.hide(mFragment).add(frameLayoutId, fragment);
                } else {
                    transaction.add(frameLayoutId, fragment);
                }
            }
            mFragment = fragment;
            transaction.commit();
        }
    }

    protected void replaceFragment(int frameLayoutId, Fragment fragment) {
        if (fragment != null) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(frameLayoutId, fragment);
            transaction.commit();
        }
    }

    protected abstract int getLayoutId();

    protected boolean initBundle(Bundle bundle) {
        return true;
    }

    protected void initWindow() {
    }

    protected void initView() {
    }

    protected void initData() {
    }

    protected void requestData() {
    }

    protected void bindEvent() {
    }

    public boolean isDestroy() {
        return mIsDestroy;
    }

}