package com.nhtzj.appb.base;


import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nhtzj.appb.interf.BaseFragmentInterface;
import com.nhtzj.common.util.XLog;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * <pre>
 *     author : Haitao
 *     e-mail : haitao_ni@foxmail.com
 *     time   : 2017/05/17
 *     desc   : 基类 Fragment <br>                已集成butterknife
 *     version: 1.0
 * </pre>
 */

public abstract class BaseFragment extends Fragment implements BaseFragmentInterface {

    protected String TAG;

    //fragment根视图
    public View rootView;
    //当前fragment绑定的acticity
    protected FragmentActivity mHolderActivity;
    //当前实例
    protected Fragment fragment;
    //butterknife实例，用于解绑View，回收资源
    private Unbinder unbinder;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TAG = getClass().getSimpleName();
        mHolderActivity = getActivity();
        fragment = this;
        initData();
        XLog.e("onCreate", TAG);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        XLog.e("onCreateView", TAG);
        rootView = inflater.inflate(getLayoutId(), container, false);
        unbinder = ButterKnife.bind(this, rootView);
        initView(rootView, savedInstanceState);
        bindEvent();
        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentActivity) {
            mHolderActivity = (FragmentActivity) context;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (unbinder != null) {
            unbinder.unbind();
        }
        XLog.e("onDestroyView", TAG);
    }

    /**
     * 获取application实例
     * @return
     */
    public Application getApplication() {
        return getActivity().getApplication();
    }

    /**
     * 获取宿主Activity
     * @return
     */
    protected FragmentActivity getHoldingActivity() {
        return mHolderActivity;
    }

    /**
     * 获取布局文件ID
     * @return
     */
    protected abstract int getLayoutId();


    @Override
    public void initView(View view, Bundle savedInstanceState) {

    }

    @Override
    public void initData() {

    }

    protected void bindEvent() {
    }

}
