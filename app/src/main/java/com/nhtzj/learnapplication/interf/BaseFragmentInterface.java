package com.nhtzj.learnapplication.interf;

import android.os.Bundle;
import android.view.View;

/**
 * <pre>
 * author : Haitao
 * blog   : http://blog.nhtzj.com
 * time   : 2018/4/11
 * desc   : Fragment基类接口
 * version: 1.0
 * </pre>
 */
public interface BaseFragmentInterface {

    void initView(View view, Bundle savedInstanceState);

    void initData();
}
