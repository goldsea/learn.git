package com.nhtzj.learnapplication.model;

import com.google.gson.annotations.SerializedName;

/**
 * <pre>
 * author : Haitao
 * blog   : http://blog.nhtzj.com
 * time   : 2018/4/11
 * desc   :
 * version: 2.0
 * </pre>
 */
public class NewsBean {
    /**
     * id : 20
     * mark : CLSDB_209701
     * title : 欧普康视：九鼎投资拟清仓减持股本的5.27%
     * content : 【欧普康视：九鼎投资拟清仓减持股本的5.27%】财联社1月25日讯，欧普康视公告，股东苏州嘉岳九鼎投资拟通过集中竞价交易、大宗交易、协议转让等方式减持本公司股份不超过6553690股，即不超过公司总股本的5.27% 。
     * reading_num : 32011
     * created_at : 2018-01-25 17:29:46
     * updated_at : 2018-01-25 17:29:46
     * unix_time : 1516872586
     */

    private int id;
    private String title;
    private String content;
    @SerializedName("reading_num")
    private String readNum;
    @SerializedName("unix_time")
    private int time;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getReadNum() {
        return readNum;
    }

    public void setReadNum(String readNum) {
        this.readNum = readNum;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }
}
