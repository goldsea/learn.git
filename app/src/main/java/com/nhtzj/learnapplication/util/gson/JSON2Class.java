package com.nhtzj.learnapplication.util.gson;


import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.Reader;
import java.lang.reflect.Type;
import java.util.List;

/**
 * <pre>
 * author : Haitao
 * e-mail : haitao_ni@foxmail.com
 * time   : 2017/8/7
 * desc   : Json字符串转class
 * version: 1.0
 * </pre>
 */
public class JSON2Class {
    private static Gson mGson = new Gson();

    public static <T> T fromJson(Reader json, Type typeOfT) throws JsonSyntaxException {
        return mGson.fromJson(json, typeOfT);
    }

    /**
     * json 字符串转Object
     *
     * @param json
     * @param typeOfT
     * @param <T>
     * @return
     * @throws JsonSyntaxException
     */
    public static <T> T fromJsonObject(String json, Class<T> typeOfT) throws JsonSyntaxException {
        if (json == null) {
            return null;
        } else {
            return mGson.fromJson(json, typeOfT);
        }
    }

    /**
     * json字符串转List<T>
     *
     * @param json
     * @param clazz
     * @param <T>
     * @return
     * @throws JsonSyntaxException
     */
    public static <T> List<T> fromJsonArray(String json, Class<T> clazz) throws JsonSyntaxException {
        if (json == null) {
            return null;
        } else {
            Type listType = new ParameterizedTypeImpl(List.class, new Class[]{clazz});
            return mGson.fromJson(json, listType);
        }
    }

    public static String toJson(Object data) {
        return mGson.toJson(data);
    }


    /**
     * 将src 序列化到本地
     *
     * @param src
     * @param writer
     */
    public static void toJson(Object src, Appendable writer) {
        mGson.toJson(src, writer);
    }

    public static Gson getGson() {
        return mGson;
    }
}
