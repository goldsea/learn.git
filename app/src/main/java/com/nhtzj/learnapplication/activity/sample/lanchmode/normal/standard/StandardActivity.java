package com.nhtzj.learnapplication.activity.sample.lanchmode.normal.standard;

import android.content.Context;
import android.content.Intent;

import com.nhtzj.learnapplication.activity.sample.lanchmode.BaseLanchModeActivity;

public class StandardActivity extends BaseLanchModeActivity {


    public static void show(Context context) {
        if (context != null) {
            context.startActivity(new Intent(context, StandardActivity.class));
        }
    }

}
