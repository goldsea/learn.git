package com.nhtzj.learnapplication.activity.sample.lanchmode.normal.singletop;

import android.content.Context;
import android.content.Intent;

import com.nhtzj.learnapplication.activity.sample.lanchmode.BaseLanchModeActivity;


public class SingleTopActivity extends BaseLanchModeActivity {

    public static void show(Context context) {
        if (context != null) {
            context.startActivity(new Intent(context, SingleTopActivity.class));
        }
    }
}
