package com.nhtzj.learnapplication.activity.main;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;

import com.nhtzj.learnapplication.activity.base.fragment.listview.BaseListFragment;
import com.nhtzj.learnapplication.activity.base.fragment.listview.ListBaseAdapter;
import com.nhtzj.learnapplication.activity.sample.FullScreenActivity;
import com.nhtzj.learnapplication.activity.sample.TextViewActivity;
import com.nhtzj.learnapplication.activity.sample.constraint.ConstraintActivity;
import com.nhtzj.learnapplication.activity.sample.dialog.DialogFragmentActivity;
import com.nhtzj.learnapplication.activity.sample.emptyview.EmptyActivity;
import com.nhtzj.learnapplication.activity.sample.lanchmode.advance.FlagClearTopActivity;
import com.nhtzj.learnapplication.activity.sample.lanchmode.advance.FlagClearTopSingleTopActivity;
import com.nhtzj.learnapplication.activity.sample.lanchmode.advance.FlagNewTaskActivity;
import com.nhtzj.learnapplication.activity.sample.lanchmode.advance.FlagNewTaskAffinityAActivity;
import com.nhtzj.learnapplication.activity.sample.lanchmode.advance.FlagNewTaskClearTaskActivity;
import com.nhtzj.learnapplication.activity.sample.lanchmode.advance.FlagNewTaskClearTopActivity;
import com.nhtzj.learnapplication.activity.sample.lanchmode.advance.FlagNewTaskClearTopAffinityBActivity;
import com.nhtzj.learnapplication.activity.sample.lanchmode.advance.FlagSingleTopActivity;
import com.nhtzj.learnapplication.activity.sample.lanchmode.advance.affinity.AffinitySingleTaskAActivity;
import com.nhtzj.learnapplication.activity.sample.lanchmode.advance.affinity.AffinitySingleTaskBActivity;
import com.nhtzj.learnapplication.activity.sample.lanchmode.advance.affinity.useless.AffinitySingleInstanceActivity;
import com.nhtzj.learnapplication.activity.sample.lanchmode.advance.affinity.useless.AffinitySingleTopAActivity;
import com.nhtzj.learnapplication.activity.sample.lanchmode.advance.affinity.useless.AffinityStandardAActivity;
import com.nhtzj.learnapplication.activity.sample.lanchmode.normal.singleinstance.SingleInstanceActivity;
import com.nhtzj.learnapplication.activity.sample.lanchmode.normal.singletask.SingleTaskActivity;
import com.nhtzj.learnapplication.activity.sample.lanchmode.normal.singletop.SingleTopActivity;
import com.nhtzj.learnapplication.activity.sample.lanchmode.normal.standard.StandardActivity;
import com.nhtzj.learnapplication.activity.sample.lv.ListViewActivity;
import com.nhtzj.learnapplication.activity.sample.richview.RichTextActivity;
import com.nhtzj.learnapplication.util.ShellUtils;

import java.util.ArrayList;

/**
 * <pre>
 * author : Haitao
 * blog   : http://blog.nhtzj.com
 * time   : 2018/4/11
 * desc   :
 * version: 2.0
 * </pre>
 */
public class MainFragment extends BaseListFragment<String> {

    private ArrayList<String> lists;


    public MainFragment() {
        // Required empty public constructor
    }

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Override
    protected boolean requestDataIfViewCreated() {
        return false;
    }

    @Override
    public boolean swipeRefreshLayoutEnadble() {
        return false;
    }

    @Override
    protected ListBaseAdapter<String> getListAdapter() {
        MainAdapter mainAdapter = new MainAdapter(mHolderActivity);
        setDatas(mainAdapter);
        return mainAdapter;
    }


    private void setDatas(MainAdapter mainAdapter) {
        ShellUtils.CommandResult commandResult = ShellUtils.execCmd("getprop ro.miui.ui.version.name", false);
        lists = new ArrayList<>();
        lists.add(mHolderActivity.toString() + "\nTaskId >>> " + mHolderActivity.getTaskId() + "\ncommandResult= " + commandResult);
        lists.add("EmptyView");
        lists.add("ListView");
        lists.add("RecycleView");

        lists.add("启动模式-standard");
        lists.add("启动模式-singleTop");
        lists.add("启动模式-singleTask");
        lists.add("启动模式-singleInstance");

        lists.add("启动模式-singleTask affinity com.nhtzj.a");
        lists.add("启动模式-singleTask affinity com.nhtzj.appb");

        lists.add("启动模式-standard 无效 affinity com.nhtzj.a");
        lists.add("启动模式-singleTop 无效 affinity com.nhtzj.a");
        lists.add("启动模式-singleInstance 无效 affinity com.nhtzj.a");

        lists.add("启动模式-Flag FLAG_ACTIVITY_NEW_TASK");
        lists.add("启动模式-Flag FLAG_ACTIVITY_NEW_TASK & affinity a");
        lists.add("启动模式-Flag FLAG_ACTIVITY_NEW_TASK & affinity appb MainActivity");
        lists.add("启动模式-Flag FLAG_ACTIVITY_NEW_TASK & clear_top");
        lists.add("启动模式-Flag FLAG_ACTIVITY_NEW_TASK & clear_task");
        lists.add("启动模式-Flag FLAG_ACTIVITY_SINGLE_TOP");
        lists.add("启动模式-Flag FLAG_ACTIVITY_CLEAR_TOP");
        lists.add("启动模式-Flag FLAG_ACTIVITY_CLEAR_TOP & single_top");

        lists.add("allowTaskReparenting");

        lists.add("启动模式-Flag FLAG_ACTIVITY_NEW_TASK & clear_top & affinity com.nhtzj.appb");

        lists.add("RichText");
        lists.add("TextView");
        lists.add("全屏切换");
        lists.add("Dialog");
        lists.add("ConstraintLayout 填坑");


        mainAdapter.setData(lists);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        super.onItemClick(parent, view, position, id);
        switch (position) {
            case 1:
                EmptyActivity.show(mHolderActivity);
                break;
            case 2:
                ListViewActivity.show(mHolderActivity);
                break;
            case 3:
                break;
            case 4:
                StandardActivity.show(mHolderActivity);
                break;
            case 5:
                SingleTopActivity.show(mHolderActivity);
                break;
            case 6:
                SingleTaskActivity.show(mHolderActivity);
                break;
            case 7:
                SingleInstanceActivity.show(mHolderActivity);
                break;
            case 8:
                AffinitySingleTaskAActivity.show(mHolderActivity);
                break;
            case 9:
                AffinitySingleTaskBActivity.show(mHolderActivity);
                break;
            case 10:
                AffinityStandardAActivity.show(mHolderActivity);
                break;
            case 11:
                AffinitySingleTopAActivity.show(mHolderActivity);
                break;
            case 12:
                AffinitySingleInstanceActivity.show(mHolderActivity);
                break;
            case 13:
                FlagNewTaskActivity.show(mHolderActivity);
                break;
            case 14:
                FlagNewTaskAffinityAActivity.show(mHolderActivity);
                break;
            case 15:
                Intent intent = new Intent();
                intent.setClassName("com.nhtzj.appb", "com.nhtzj.appb.sample.MainActivity");
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                break;
            case 16:
                FlagNewTaskClearTopActivity.show(mHolderActivity);
                break;
            case 17:
                FlagNewTaskClearTaskActivity.show(mHolderActivity);
                break;
            case 18:
                FlagSingleTopActivity.show(mHolderActivity);
                break;
            case 19:
                FlagClearTopActivity.show(mHolderActivity);
                break;
            case 20:
                FlagClearTopSingleTopActivity.show(mHolderActivity);
                break;
            case 21:
                Intent intent2 = new Intent();
                intent2.setClassName("com.nhtzj.appb", "com.nhtzj.appb.sample.TestReparentActivity");
                startActivity(intent2);
                break;
            case 22:
                FlagNewTaskClearTopAffinityBActivity.show(mHolderActivity);
                break;
            case 23:
                RichTextActivity.show(mHolderActivity);
                break;
            case 24:
                TextViewActivity.show(mHolderActivity);
                break;
            case 25:
                FullScreenActivity.show(mHolderActivity);
                break;
            case 26:
                DialogFragmentActivity.show(mHolderActivity);
                break;
            case 27:
                ConstraintActivity.show(mHolderActivity);
                break;
        }
    }
}
