package com.nhtzj.learnapplication.activity.base.activity;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.nhtzj.learnapplication.R;

public class BaseFragmentActivity extends BaseAppCompatActivity {

    @Override
    protected int getLayoutId() {
        return R.layout.activity_base_fragment;
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    protected void replaceFragment(Fragment fragment) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        transaction.replace(R.id.fl_contain, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}