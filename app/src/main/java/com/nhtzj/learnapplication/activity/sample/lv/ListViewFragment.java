package com.nhtzj.learnapplication.activity.sample.lv;

import com.nhtzj.common.okhttp.sec.OHS3;
import com.nhtzj.learnapplication.activity.base.fragment.listview.BaseListFragment;
import com.nhtzj.learnapplication.activity.base.fragment.listview.ListBaseAdapter;
import com.nhtzj.learnapplication.model.NewsBean;
import com.nhtzj.learnapplication.util.gson.JSON2Class;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

/**
 * <pre>
 * author : Haitao
 * blog   : http://blog.nhtzj.com
 * time   : 2018/4/11
 * desc   :
 * version: 2.0
 * </pre>
 */
public class ListViewFragment extends BaseListFragment<NewsBean> {

    private HashMap<String, String> params;


    public ListViewFragment() {
        // Required empty public constructor
    }

    public static ListViewFragment newInstance() {
        return new ListViewFragment();
    }


    @Override
    protected ListBaseAdapter<NewsBean> getListAdapter() {
        return new NewsAdapter(mHolderActivity);
    }

    @Override
    protected void sendRequestData() {
        super.sendRequestData();
        if (params == null) {
            params = new HashMap<>();
        }
        params.put("page", String.valueOf(mCurrentPage));
        params.put("per_page", String.valueOf(mAdapter.getPageSize()));
        OHS3.postJoBack(mHolderActivity, "http://api.jafso.com/api/news", params, mCallBack);
    }

    @Override
    protected List<NewsBean> parseList(JSONObject dataPacket) throws Exception {
        return JSON2Class.fromJsonArray(dataPacket.optString("data", "[]"), NewsBean.class);
    }
}
