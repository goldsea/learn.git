package com.nhtzj.learnapplication.activity.sample.lanchmode.advance;

import android.content.Context;
import android.content.Intent;

import com.nhtzj.learnapplication.activity.sample.lanchmode.BaseLanchModeActivity;

/**
 * <pre>
 * author : Haitao
 * blog   : http://blog.nhtzj.com
 * time   : 2018/4/20
 * desc   :
 * version: 2.0
 * </pre>
 */
public class FlagNewTaskClearTopActivity extends BaseLanchModeActivity {
    public static void show(Context context) {
        if (context != null) {
            context.startActivity(new Intent(context, FlagNewTaskClearTopActivity.class)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP));
        }

    }
}
