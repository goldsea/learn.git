package com.nhtzj.learnapplication.activity.main;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nhtzj.learnapplication.R;
import com.nhtzj.learnapplication.activity.base.fragment.listview.ListBaseAdapter;
import com.nhtzj.learnapplication.model.NewsBean;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * <pre>
 * author : Haitao
 * blog   : http://blog.nhtzj.com
 * time   : 2018/4/11
 * desc   :
 * version: 2.0
 * </pre>
 */
public class MainAdapter extends ListBaseAdapter<String> {
    private Context mContext;
    private LayoutInflater mInflater;

    public MainAdapter(Context mContext) {
        this.mContext = mContext;
        mInflater = LayoutInflater.from(mContext);
    }

    @Override
    protected View getRealView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (null == convertView || convertView.getTag() == null) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.item_news, null);
            holder.tvTitle = (TextView) convertView.findViewById(R.id.tv_title);
            convertView.findViewById(R.id.tv_time).setVisibility(View.GONE);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        initView(position, holder);
        return convertView;
    }

    private void initView(int position, ViewHolder holder) {
        holder.tvTitle.setText(getItem(position));
    }

    private class ViewHolder {
        TextView tvTitle;
    }
}
