package com.nhtzj.learnapplication.activity.base.fragment.listview;


import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;

import okhttp3.Call;

import android.view.View;
import android.view.ViewStub;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;


import com.nhtzj.common.okhttp.sec.OkJoCallback;
import com.nhtzj.common.util.XLog;
import com.nhtzj.learnapplication.R;
import com.nhtzj.learnapplication.widget.empty.EmptyLayout;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


@SuppressLint("NewApi")
public abstract class BaseListFragment<T> extends BaseFragment
        implements AdapterView.OnItemClickListener,
        AbsListView.OnScrollListener, AdapterView.OnItemLongClickListener, SwipeRefreshLayout.OnRefreshListener {

    protected EmptyLayout mEmptyLayout;

    private SwipeRefreshLayout mSwipeRefreshLayout;

    protected ListView mListView;

    protected ListBaseAdapter<T> mAdapter;

    protected int mStoreEmptyState = -1;

    protected int mCurrentPage = getFirstPageIndex();

    private ParserTask mParserTask;

    private DataRequesting onDataRequestingListener;

    protected ViewStub vsBottom;

    /**
     * 获取首页的下标，通过重写该方法设置新值
     *
     * @author nht
     * @add_annotation_time 2016/11/11 15:24
     */
    protected int getFirstPageIndex() {
        return 1;
    }

    public int getCurrentPage() {
        return mCurrentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.mCurrentPage = currentPage;
    }

    /**
     * 界面加载完后是否自动请求数据<br>
     * 默认 true
     *
     * @author nht
     * @add_annotation_time 2016/11/11 15:14
     */
    protected boolean requestDataIfViewCreated() {
        return true;
    }


    /**
     * 是否需要隐藏listview，显示无数据状态
     */
    protected boolean needShowEmptyNoData() {
        return true;
    }

    protected boolean needShowLoading() {
        return true;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_pull_refresh_listview;
    }


    @Override
    public void initView(View view, Bundle savedInstanceState) {

        mEmptyLayout = (EmptyLayout) view.findViewById(R.id.innererrorlayout);

        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swiperefreshlayout);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeResources(
                R.color.swiperefresh_color1, R.color.swiperefresh_color2,
                R.color.swiperefresh_color3, R.color.swiperefresh_color4);
        mSwipeRefreshLayout.setEnabled(swipeRefreshLayoutEnadble());

        mListView = (ListView) view.findViewById(R.id.listview);

        mEmptyLayout.setOnLayoutClickListener(new EmptyLayout.OnViewClick() {

            @Override
            public void onViewClick(View v) {
                mCurrentPage = getFirstPageIndex();
                mState = STATE_REFRESH;
                mEmptyLayout.setErrorType(EmptyLayout.NETWORK_LOADING);
                sendRequestData();
            }
        });
        addHeaderView(mListView);
        mListView.setOnItemClickListener(this);
        mListView.setOnItemLongClickListener(this);

        mListView.setOnScrollListener(this);

        if (mAdapter != null) {
            mListView.setAdapter(mAdapter);
            mEmptyLayout.setErrorType(EmptyLayout.HIDE_LAYOUT);
        } else {
            mAdapter = getListAdapter();
            mListView.setAdapter(mAdapter);

            if (requestDataIfViewCreated()) {
                if (needShowLoading()) {
                    mEmptyLayout.setErrorType(EmptyLayout.NETWORK_LOADING);
                }
                mState = STATE_NONE;
                sendRequestData();
            } else {
                mEmptyLayout.setErrorType(EmptyLayout.HIDE_LAYOUT);
            }

        }
        vsBottom = (ViewStub) view.findViewById(R.id.vs_bottom);
        initVsBottom(vsBottom);

        if (mStoreEmptyState != -1) {
            mEmptyLayout.setErrorType(mStoreEmptyState);
        }


    }

    protected void initVsBottom(ViewStub vsBottom) {

    }

    @Override
    public void onDestroyView() {
        mStoreEmptyState = mEmptyLayout.getErrorState();
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        cancelParserTask();
        super.onDestroy();
    }

    /**
     * 获取列表适配器<br> 子类必须实现
     *
     * @author nht
     * @add_annotation_time 2016/11/11 15:15
     */
    protected abstract ListBaseAdapter<T> getListAdapter();


    public boolean swipeRefreshLayoutEnadble() {
        return true;
    }

    // 下拉刷新数据
    @Override
    public void onRefresh() {
        if (mState == STATE_REFRESH) {
            return;
        }
        // 设置顶部正在刷新
        mListView.setSelection(0);
        setSwipeRefreshLoadingState();
        mCurrentPage = getFirstPageIndex();
        mState = STATE_REFRESH;
//        requestData(true);
        sendRequestData();

        if (onDataRequestingListener != null) {
            onDataRequestingListener.onDataRequesting(true);
        }

    }

    /**
     * 供子类继承，解析数据<P>
     * 注：该操作在子线程中，若有UI操作，请自行处理
     *
     * @author nht
     * @add_annotation_time 2016/11/11 15:12
     */
    protected List<T> parseList(JSONObject dataPacket) throws Exception {
        return null;
    }

    /**
     * 添加列表头部视图
     *
     * @author nht
     * @add_annotation_time 2016/11/11 15:11
     */
    protected void addHeaderView(ListView listView) {
    }

    protected void setDividerColor(ListView listView) {

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
                            long id) {
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        return false;
    }

    /**
     * 是否需要自动刷新<p>
     * 该功能还未编写
     *
     * @author nht
     * @add_annotation_time 2016/11/11 15:10
     */
    protected boolean needAutoRefresh() {
        return true;
    }

    /**
     * 自动刷新的时间
     * <p/>
     * 默认：自动刷新的时间为半天时间
     *
     * @return
     */
//    protected long getAutoRefreshTime() {
//        return 12 * 60 * 60;
//    }
    @Override
    public void onResume() {
        super.onResume();
//        if (onTimeRefresh()) {
//            onRefresh();
//        }
    }

    /**
     * 发送刷新请求<p>
     * 具体请求方式和参数写在该方法内<p>
     */
    protected void sendRequestData() {
    }


    protected void autoRefresh() {
        setSwipeRefreshLoadingState();
        onRefresh();
    }

    protected void autoRefreshWithEmptyLayout() {
        mCurrentPage = getFirstPageIndex();
        mState = STATE_REFRESH;
        mEmptyLayout.setErrorType(EmptyLayout.NETWORK_LOADING);
        sendRequestData();
    }

    /**
     * 无动画刷新列表
     */
    protected void autoRefreshWithoutShow() {
        mCurrentPage = getFirstPageIndex();
        mState = STATE_REFRESH;
//        mErrorLayout.setErrorType(ErrorLayout.NETWORK_LOADING);
        sendRequestData();
    }


    /**
     * 网络回调<p>
     * sendRequestData、sendRequestNextData 传入此参数
     */
    protected OkJoCallback mCallBack = new OkJoCallback(false) {
        @Override
        public void onDataSuccess(JSONObject dataPacket) throws Exception {
            executeParserTask(dataPacket);
        }

        @Override
        public void onDataError(int errorCode, String errorMsg, int id) {
            super.onDataError(errorCode, errorMsg, id);
            executeOnErrorWithEV();
        }

        @Override
        public void onNetError(Call call, Exception e, int id, int code) {
            super.onNetError(call, e, id, code);

            if (code == 401) {
                executeOnNoLogin();
            } else {
                executeOnErrorWithEV();
            }
        }
    };

    /**
     * 数据处理，状态更新
     */
    protected void executeOnLoadDataSuccess(List<T> data) {
        if (data == null) {
            data = new ArrayList<T>();
        }
        if (mEmptyLayout.getErrorState() != EmptyLayout.HIDE_LAYOUT) {

            mEmptyLayout.setErrorType(EmptyLayout.HIDE_LAYOUT);
        }

        data = processSuccessData(data, mAdapter.getData());

        if (mCurrentPage == getFirstPageIndex()) {
            mAdapter.clear();
            onDataProcessed();
        }

        int adapterState;
        if ((mAdapter.getCount() + data.size()) == 0) {
            adapterState = ListBaseAdapter.STATE_EMPTY_ITEM;
        } else if (data.size() == 0 || (data.size() < getPageSize())) {
            adapterState = ListBaseAdapter.STATE_NO_MORE;
            mAdapter.notifyDataSetChanged();
            if (data.size() == 0) {
                mCurrentPage--;
            }
        } else {
            adapterState = ListBaseAdapter.STATE_LOAD_MORE;
        }
        if (mAdapter.hasFooterView()) {
            mAdapter.setState(adapterState);
        }
        mAdapter.addData(data);
        // 判断等于是因为最后有一项是listview的状态
        if (mAdapter.getCount() == 1 && mAdapter.hasFooterView() || (mAdapter.getCount() == 0 && !mAdapter.hasFooterView())) {

            if (needShowEmptyNoData()) {
                mEmptyLayout.setErrorType(EmptyLayout.NODATA);
            } else {
                mAdapter.setState(ListBaseAdapter.STATE_EMPTY_ITEM);
                mAdapter.notifyDataSetChanged();
            }
        }

        executeOnLoadFinish();
    }


    /**
     * 数据加载、处理完毕
     */
    protected List<T> processSuccessData(List<T> newData, List<T> oldData) {
        return newData;
    }

    protected void onDataProcessed() {

    }

    /**
     * 单次加载数据条数<p>
     * 默认：10
     */
    protected int getPageSize() {
        return 10;
    }

    /**
     * 数据加载失败，界面显示
     */
    protected void executeOnLoadDataError() {

        //在无网络时，滚动到底部时，mCurrentPage先自加了，然而在失败时却
        //没有减回来，如果刻意在无网络的情况下上拉，可以出现漏页问题
        mCurrentPage--;
        if (mCurrentPage < getFirstPageIndex()) {
            mCurrentPage = getFirstPageIndex();
        }

        mEmptyLayout.setErrorType(EmptyLayout.HIDE_LAYOUT);
        mAdapter.setState(ListBaseAdapter.STATE_NETWORK_ERROR);
        mAdapter.notifyDataSetChanged();

        executeOnLoadFinish();

        onDataProcessed();
    }

    /**
     * 提示未登录状态
     */
    protected void executeOnNoLogin() {

        mEmptyLayout.setErrorType(EmptyLayout.NO_LOGIN);
        mEmptyLayout.setErrorMessage(getString(R.string.tip_unlogin));
        mAdapter.setState(ListBaseAdapter.STATE_NETWORK_ERROR);
        onDataProcessed();
    }

    /**
     * 数据加载失败，使用ErrorLayout显示失败结果
     */
    protected void executeOnErrorWithEV() {

        mCurrentPage--;
        if (mCurrentPage < getFirstPageIndex()) {
            mCurrentPage = getFirstPageIndex();
        }

        mEmptyLayout.setErrorType(EmptyLayout.NETWORK_ERROR);
//        mErrorLayout.setErrorMessage(getString(R.string.error_data_process));
        mAdapter.setState(ListBaseAdapter.STATE_NETWORK_ERROR);
        mAdapter.notifyDataSetChanged();

        executeOnLoadFinish();

        onDataProcessed();
    }


    /**
     * 完成刷新，界面状态刷新
     */
    protected void executeOnLoadFinish() {
        setSwipeRefreshLoadedState();
        mState = STATE_NONE;
        if (onDataRequestingListener != null) {
            onDataRequestingListener.onDataRequesting(false);
        }
    }

    /**
     * 设置顶部正在加载的状态
     */
    protected void setSwipeRefreshLoadingState() {
        if (mSwipeRefreshLayout != null) {
            mSwipeRefreshLayout.setRefreshing(true);
            // 防止多次重复刷新
            mSwipeRefreshLayout.setEnabled(false);
        }

    }

    /**
     * 设置顶部加载完毕的状态
     */
    protected void setSwipeRefreshLoadedState() {
        if (mSwipeRefreshLayout != null) {
            mSwipeRefreshLayout.setRefreshing(false);
            mSwipeRefreshLayout.setEnabled(swipeRefreshLayoutEnadble());
        }
    }

    /**
     * 执行数据解析
     */
    private void executeParserTask(JSONObject data) {
        cancelParserTask();
        mParserTask = new ParserTask(data);
        mParserTask.execute();
    }

    /**
     * 取消数据解析
     */
    private void cancelParserTask() {
        if (mParserTask != null) {
            mParserTask.cancel(true);
            mParserTask = null;
        }
    }


    /**
     * 异步解析数据
     * <br>
     * executeOnLoadDataError :解析出错
     * <br>
     * executeOnLoadDataSuccess(list) :解析成功
     */
    class ParserTask extends AsyncTask<Void, Void, String> {

        private final JSONObject reponseData;
        private boolean parserError;
        private List<T> list;

        public ParserTask(JSONObject data) {
            this.reponseData = data;
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                list = parseList(reponseData);
            } catch (Exception e) {
                e.printStackTrace();

                XLog.e(e.getMessage() + "");

                parserError = true;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (parserError) {
                executeOnLoadDataError();
            } else {
                executeOnLoadDataSuccess(list);
            }
        }
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        if (mAdapter == null || mAdapter.getCount() == 0) {
            return;
        }
        // 数据已经全部加载，或数据为空时，或正在加载，不处理滚动事件
        if (mState == STATE_LOADMORE || mState == STATE_REFRESH) {
            return;
        }
        // 判断是否滚动到底部
        boolean scrollEnd = false;
        try {
            if (view.getPositionForView(mAdapter.getFooterView()) == view
                    .getLastVisiblePosition())
                scrollEnd = true;
        } catch (Exception e) {
            scrollEnd = false;
        }

        if (mState == STATE_NONE && scrollEnd) {
            if (mAdapter.getState() == ListBaseAdapter.STATE_LOAD_MORE
                    || mAdapter.getState() == ListBaseAdapter.STATE_NETWORK_ERROR) {
                mCurrentPage++;
                mState = STATE_LOADMORE;
                sendRequestData();
                mAdapter.setFooterViewLoading();
                if (onDataRequestingListener != null) {
                    onDataRequestingListener.onDataRequesting(true);
                }
            }
        }
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem,
                         int visibleItemCount, int totalItemCount) {
    }

    protected void getNextDataByUser() {
        mCurrentPage++;
        mState = STATE_LOADMORE;
        sendRequestData();
        mAdapter.setFooterViewLoading();
        if (onDataRequestingListener != null) {
            onDataRequestingListener.onDataRequesting(true);
        }
    }


    public void setOnDataRequestingListener(DataRequesting onDataRequestingListener) {
        this.onDataRequestingListener = onDataRequestingListener;
    }

    /**
     * 该接口用于获取列表当前状态<br>
     * 子类可直接取mState值进行判断，无需实现该接口
     */
    public interface DataRequesting {
        /**
         * @param requesting true:正在请求、处理数据<br>
         *                   false:处理完毕（处理失败，处理成功，无网络等）
         */
        void onDataRequesting(boolean requesting);
    }

}
