package com.nhtzj.learnapplication.activity.sample;

import android.app.Activity;
import android.os.Bundle;

import com.nhtzj.learnapplication.R;

public class ConstraintLayoutActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_constraint_layout);
    }
}
