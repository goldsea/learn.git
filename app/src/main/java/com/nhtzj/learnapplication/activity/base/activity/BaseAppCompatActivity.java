package com.nhtzj.learnapplication.activity.base.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.nhtzj.common.util.XLog;
import com.nhtzj.learnapplication.R;
import com.nhtzj.learnapplication.interf.IDialogControl;
import com.nhtzj.learnapplication.util.DialogHelper;

import butterknife.ButterKnife;

/**
 * <pre>
 * author : Haitao
 * blog   : http://blog.nhtzj.com
 * time   : 2018/4/11
 * desc   :
 * version: 2.0
 * </pre>
 */
public abstract class BaseAppCompatActivity extends AppCompatActivity implements IDialogControl {
    protected String TAG;
    private boolean mIsDestroy;
    private Fragment mFragment;
    public BaseAppCompatActivity activityInstance;

    private ProgressDialog _waitDialog;

    private Handler mHandler = new Handler();

    /**
     * 隐藏系统状态栏和导航栏
     */
    private Runnable hideSystemUIAction = this::hideSystemUI;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TAG = this.getClass().getSimpleName();

        XLog.e(TAG, "onCreate");
        if (initBundle(getIntent().getExtras())) {
            setContentView(getLayoutId());
            activityInstance = this;
            initWindow();
            initData();
            ButterKnife.bind(this);
            initView();
            requestData();
            bindEvent();
        } else {
            finish();
        }

    }


    @Override
    protected void onStart() {
        super.onStart();
        XLog.e(TAG, "onStart");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        XLog.e(TAG, "onRestart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        XLog.e(TAG, "onResume");
        View decorView = getWindow().getDecorView();
        //注册监听器，以便让应用获得系统界面可见性更改的通知
        decorView.setOnSystemUiVisibilityChangeListener(this::onSystemUiVisibilityChange);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        XLog.e(TAG, "onNewIntent");
    }

    @Override
    protected void onPause() {
        super.onPause();
        XLog.e(TAG, "onPause");
        View decorView = getWindow().getDecorView();
        //取消注册监听器
        decorView.setOnSystemUiVisibilityChangeListener(null);
    }

    @Override
    protected void onStop() {
        super.onStop();
        XLog.e(TAG, "onStop");
    }

    @Override
    protected void onDestroy() {
        XLog.e(TAG, "onDestroy");
        mIsDestroy = true;
        super.onDestroy();
        activityInstance = null;
    }

    /**
     * 当前activity所属的Window得到/失去焦点时会回调该方法
     *
     * @param hasFocus
     */
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        boolean orientationLandscape = isOrientationLandscape(getResources());
        Log.w(TAG, String.format("onWindowFocusChanged: %s isLandscape:%s", hasFocus, orientationLandscape));
        if (hasFocus && orientationLandscape) {
            hideSystemUI();
        }
    }

    /**
     * {@link View.OnSystemUiVisibilityChangeListener} 接口的回调操作，setOnSystemUiVisibilityChangeListener设置的哦
     *
     * @param visibility
     */
    public void onSystemUiVisibilityChange(int visibility) {
        // Note that system bars will only be "visible" if none of the
        // LOW_PROFILE, HIDE_NAVIGATION, or FULLSCREEN flags are set.

        boolean orientationLandscape = isOrientationLandscape(getResources());

        if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) { //系统状态栏显示
            if (orientationLandscape) {
                mHandler.removeCallbacks(hideSystemUIAction);
                mHandler.postDelayed(hideSystemUIAction, 100);

            }

        } else {//系统状态栏隐藏

        }
    }

    /**
     * 系统配置更新回调
     * @param newConfig
     */
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (ActivityInfo.SCREEN_ORIENTATION_USER == newConfig.orientation) { //横屏
            hideSystemUI();
            mHandler.postDelayed(hideSystemUIAction, 500);
        } else { //竖屏
            mHandler.removeCallbacks(hideSystemUIAction);
            showSystemUI();
        }
    }

    /**
     * 隐藏系统状态栏和导航栏
     */
    private void hideSystemUI() {
        //使用SYSTEM_UI_FLAG_IMMERSIVE_STICKY模式，
        // 即：粘性沉浸模式，如果用户从隐藏了系统栏的边缘滑动，系统栏会显示出来，但它们是半透明的，
        // 并且轻触手势会传递给应用，因此应用也会响应该手势
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    /**
     * 显示系统状态栏和导航栏
     */
    private void showSystemUI() {
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_VISIBLE);
    }

    /**
     * 判断当前界面是否为横屏
     *
     * @param resources
     * @return
     */
    protected boolean isOrientationLandscape(Resources resources) {
        if (resources == null) {
            return false;
        }
        return Configuration.ORIENTATION_LANDSCAPE == resources.getConfiguration().orientation;
    }

    protected void addFragment(int frameLayoutId, Fragment fragment) {
        if (fragment != null) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            if (fragment.isAdded()) {
                if (mFragment != null) {
                    transaction.hide(mFragment).show(fragment);
                } else {
                    transaction.show(fragment);
                }
            } else {
                if (mFragment != null) {
                    transaction.hide(mFragment).add(frameLayoutId, fragment);
                } else {
                    transaction.add(frameLayoutId, fragment);
                }
            }
            mFragment = fragment;
            transaction.commit();
        }
    }

    protected void replaceFragment(int frameLayoutId, Fragment fragment) {
        if (fragment != null) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(frameLayoutId, fragment);
            transaction.commit();
        }
    }

    protected abstract int getLayoutId();

    protected boolean initBundle(Bundle bundle) {
        return true;
    }

    protected void initWindow() {
    }

    protected void initView() {
    }

    protected void initData() {
    }

    protected void requestData() {
    }

    protected void bindEvent() {
    }

    public boolean isDestroy() {
        return mIsDestroy;
    }

    /**
     * 显示加载等待视图,显示"加载中..."
     *
     * @return
     */
    @Override
    public ProgressDialog showWaitDialog() {
        return showWaitDialog(R.string.loading);
    }

    /**
     * 显示加载等待视图,显示传入字符串资源值
     *
     * @param resid
     * @return
     */
    @Override
    public ProgressDialog showWaitDialog(int resid) {
        return showWaitDialog(getString(resid));
    }

    /**
     * 显示加载等待视图,显示传入字符串
     *
     * @param message
     * @return
     */
    @Override
    public ProgressDialog showWaitDialog(String message) {
        if (_waitDialog == null) {
            _waitDialog = DialogHelper.getProgressDialog(this, message);
        }
        if (_waitDialog != null) {
            _waitDialog.setMessage(message);
            _waitDialog.show();
        }
        return _waitDialog;
    }

    /**
     * 隐藏加载等待视图
     */
    @Override
    public void hideWaitDialog() {
        if (_waitDialog != null && _waitDialog.isShowing()) {
            try {
                _waitDialog.dismiss();
                _waitDialog = null;
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
}