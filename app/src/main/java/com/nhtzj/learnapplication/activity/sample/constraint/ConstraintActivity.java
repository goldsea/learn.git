package com.nhtzj.learnapplication.activity.sample.constraint;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.view.View;
import android.widget.Button;

import com.nhtzj.learnapplication.R;
import com.nhtzj.learnapplication.activity.base.activity.BaseAppCompatActivity;

public class ConstraintActivity extends BaseAppCompatActivity {

    private ConstraintLayout constraintContainer;
    private View vBigGreen;
    private View vSmallBlue;
    private Button btnFullscreen;


    private ConstraintSet constraintSet;


    public static void show(Context context) {
        if (context != null) {
            context.startActivity(new Intent(context, ConstraintActivity.class));
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_constraint;
    }

    @Override
    protected void initView() {
        super.initView();
        constraintContainer = findViewById(R.id.constraint_container);
        vBigGreen = findViewById(R.id.v_big_green);
        vSmallBlue = findViewById(R.id.v_small_blue);
        btnFullscreen = findViewById(R.id.btn_fullscreen);

        constraintSet = new ConstraintSet();
        constraintSet.clone(constraintContainer);
    }

    @Override
    protected void bindEvent() {
        super.bindEvent();
        btnFullscreen.setOnClickListener(this::onClickBtnFullscreen);
        vBigGreen.setOnClickListener(v -> {

            boolean preVisible = vSmallBlue.getVisibility() == View.VISIBLE;
            vSmallBlue.setVisibility(preVisible ? View.GONE : View.VISIBLE);
            //使用constraintSet进行参数调整时，必须添加如下修改
            constraintSet.setVisibility(R.id.v_small_blue, preVisible ? ConstraintSet.GONE : ConstraintSet.VISIBLE);
        });
    }

    /**
     * 全屏按钮点击事件处理
     *
     * @param view
     */
    @SuppressLint("SourceLockedOrientationActivity")
    private void onClickBtnFullscreen(View view) {
        boolean orientationLandscape = isOrientationLandscape(getResources());
        if (orientationLandscape) {
            btnFullscreen.setText("全屏");
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else {
            btnFullscreen.setText("退出全屏");
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == ActivityInfo.SCREEN_ORIENTATION_USER) {//横屏
            constraintSet.setDimensionRatio(R.id.v_big_green, "");
        } else {
            constraintSet.setDimensionRatio(R.id.v_big_green, "16:9");
        }
        constraintSet.applyTo(constraintContainer);
    }
}
