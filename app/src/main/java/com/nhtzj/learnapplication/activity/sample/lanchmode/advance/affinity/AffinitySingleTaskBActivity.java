package com.nhtzj.learnapplication.activity.sample.lanchmode.advance.affinity;

import android.content.Context;
import android.content.Intent;

import com.nhtzj.learnapplication.activity.sample.lanchmode.BaseLanchModeActivity;

/**
 * <pre>
 * author : Haitao
 * blog   : http://blog.nhtzj.com
 * time   : 2018/4/20
 * desc   :
 * version: 2.0
 * </pre>
 */
public class AffinitySingleTaskBActivity extends BaseLanchModeActivity {
    public static void show(Context context) {
        if (context != null) {
            context.startActivity(new Intent(context, AffinitySingleTaskBActivity.class));
        }

    }
}
