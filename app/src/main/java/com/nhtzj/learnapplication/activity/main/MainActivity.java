package com.nhtzj.learnapplication.activity.main;

import com.nhtzj.learnapplication.activity.base.activity.BaseFragmentActivity;

public class MainActivity extends BaseFragmentActivity {

    @Override
    protected void initView() {
        super.initView();
        replaceFragment(MainFragment.newInstance());
    }
}
