package com.nhtzj.learnapplication.activity.sample.dialog;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.nhtzj.learnapplication.R;
import com.nhtzj.learnapplication.activity.base.activity.BaseAppCompatActivity;
import com.nhtzj.learnapplication.activity.sample.dialog.dialogfragment.SimpleDialogFragment;

/**
 * DialogFragment外部点击事件监听 示例
 */
public class DialogFragmentActivity extends BaseAppCompatActivity {

    private Button btnShowDialog;

    public static void show(Context context) {
        if (context != null) {
            context.startActivity(new Intent(context, DialogFragmentActivity.class));
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_dialog_fragment;
    }

    @Override
    protected void initView() {
        super.initView();
        btnShowDialog = findViewById(R.id.btn_show_dialog);
    }

    @Override
    protected void bindEvent() {
        super.bindEvent();
        btnShowDialog.setOnClickListener(this::showDialog);
    }

    /**
     * btnShowDialog 点击事件回调，显示弹框
     * @param v
     */
    private void showDialog(View v) {
        new SimpleDialogFragment.Builder()
                .setOutsideClickListener(() -> {
                    Log.w(TAG, "showDialog outer click ");
                    Toast.makeText(activityInstance, "outer click", Toast.LENGTH_SHORT).show();
                    return true;
                })
                .setOnDismissListener(dialog -> {
                    Log.w(TAG, "showDialog dismiss");
                })
        .show(getSupportFragmentManager(), "simple_dialog_test");
    }
}
