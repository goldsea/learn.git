package com.nhtzj.learnapplication.activity.sample.richview;

import android.content.Context;
import android.content.Intent;

import com.nhtzj.common.util.ToastUtils;
import com.nhtzj.learnapplication.R;
import com.nhtzj.learnapplication.activity.base.activity.BaseAppCompatActivity;
import com.nhtzj.learnapplication.widget.richtext.RichText;

import java.util.List;

import butterknife.BindView;

public class RichTextActivity extends BaseAppCompatActivity {

    @BindView(R.id.richtest)
    RichText richtest;

    public static void show(Context context) {
        if (context != null) {
            context.startActivity(new Intent(context, RichTextActivity.class));
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_rich_text;
    }

    @Override
    protected void initView() {
        super.initView();
        String text = "<html><body><a href='http://www.nhtzj.com/'>博客首页</a><p>RichText 简单富文本，实现图片加载和链接</p><img src='http://www.nhtzj.com//images/2018/05/robots测试工具.png'><img src='http://www.nhtzj.com//images/2018/05/Handler简要说明.png'><img src='http://www.nhtzj.com//images/2018/05/Handler工作流程图／消息队列模型.png'></body></html>";
        richtest.setRichText(text);
    }

    @Override
    protected void bindEvent() {
        super.bindEvent();
        richtest.setOnImageClickListener(new RichText.OnImageClickListener() {
            @Override
            public void imageClicked(List<String> imageUrls, int position) {
                ToastUtils.makeText(imageUrls.get(position));

            }

        });

        richtest.setOnUrlClickListenerListener(new RichText.OnUrlClickListener() {
            @Override
            public void urlClicked(String url) {
                ToastUtils.makeText(url);
            }
        });
    }
}
