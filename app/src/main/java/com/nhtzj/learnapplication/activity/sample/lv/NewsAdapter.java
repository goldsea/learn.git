package com.nhtzj.learnapplication.activity.sample.lv;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nhtzj.learnapplication.R;
import com.nhtzj.learnapplication.activity.base.fragment.listview.ListBaseAdapter;
import com.nhtzj.learnapplication.model.NewsBean;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * <pre>
 * author : Haitao
 * blog   : http://blog.nhtzj.com
 * time   : 2018/4/11
 * desc   :
 * version: 2.0
 * </pre>
 */
public class NewsAdapter extends ListBaseAdapter<NewsBean> {
    private Context mContext;
    private LayoutInflater mInflater;
    private final DateFormat dateFormat;

    public NewsAdapter(Context mContext) {
        this.mContext = mContext;
        mInflater=LayoutInflater.from(mContext);
        state = STATE_LOAD_MORE;
        dateFormat = new SimpleDateFormat("MM-dd HH:mm", Locale.getDefault());
    }

    @Override
    protected View getRealView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (null == convertView || convertView.getTag() == null) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.item_news, null);
            holder.tvTitle = (TextView) convertView.findViewById(R.id.tv_title);
            holder.tvTime = (TextView) convertView.findViewById(R.id.tv_time);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        initView(position, holder);
        return convertView;
    }

    private void initView(int position, ViewHolder holder) {
        NewsBean newsBean = getItem(position);
        holder.tvTitle.setText(newsBean.getTitle());
        holder.tvTime.setText(dateFormat.format(new Date(newsBean.getTime())));
    }

    private class ViewHolder {
        TextView tvTitle, tvTime;
    }
}
