package com.nhtzj.learnapplication.activity.sample.lanchmode.normal.singleinstance;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.nhtzj.learnapplication.activity.sample.lanchmode.BaseLanchModeActivity;

public class SingleInstanceActivity extends BaseLanchModeActivity {

    public static void show(Context context) {
        if (context != null) {
            context.startActivity(new Intent(context, SingleInstanceActivity.class));
        }

    }

}
