package com.nhtzj.learnapplication.activity.sample.lanchmode;

import android.content.Intent;

import com.nhtzj.learnapplication.activity.base.activity.BaseFragmentActivity;

/**
 * <pre>
 * author : Haitao
 * blog   : http://blog.nhtzj.com
 * time   : 2018/4/20
 * desc   :
 * version: 2.0
 * </pre>
 */
public class BaseLanchModeActivity extends BaseFragmentActivity {
    private LanchModeFragment fragment;

    @Override
    protected void initView() {
        super.initView();
        fragment = LanchModeFragment.newInstance();
        replaceFragment(fragment);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        fragment.onNewIntent();
    }
}
