package com.nhtzj.learnapplication.activity;

import android.app.Application;
import android.content.Context;

import com.nhtzj.common.okhttp.OkHttpUtils3;
import com.nhtzj.common.okhttp.interceptor.LoggerInterceptor;
import com.nhtzj.common.okhttp.interceptor.RetryIntercepter;
import com.nhtzj.common.util.ContextUtils;
import com.nhtzj.common.util.XLog;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;

import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

import okhttp3.OkHttpClient;

/**
 * <pre>
 * author : Haitao
 * blog   : http://blog.nhtzj.com
 * time   : 2018/4/11
 * desc   :
 * version: 2.0
 * </pre>
 */
public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        ContextUtils.init(this);
        initOkHttp();
        initImageLoader(this);
    }

    private void initOkHttp() {
        try {

            OkHttpUtils3.initClient(new OkHttpClient.Builder()
                    .connectTimeout(20000L, TimeUnit.MILLISECONDS)
                    .readTimeout(20000L, TimeUnit.MILLISECONDS)
                    .addInterceptor(new RetryIntercepter(2))
                    .addInterceptor(new LoggerInterceptor("TAG", true))

                    .hostnameVerifier(new HostnameVerifier() {
                        @Override
                        public boolean verify(final String s, final SSLSession sslSession) {
                            return true;
                        }
                    })
                    .build());
        } catch (Exception ex) {
            XLog.e("App", "initOKHttp", ex);
        }

    }

    public static void initImageLoader(Context context) {
        int NORM_PRIORITY = Thread.NORM_PRIORITY;
        ImageLoaderConfiguration configuration = new ImageLoaderConfiguration
                .Builder(context)
                .memoryCacheExtraOptions(480, 800)
                .threadPoolSize(4)
                .threadPriority(NORM_PRIORITY - 1 == 0 ? NORM_PRIORITY : NORM_PRIORITY - 1)
                .denyCacheImageMultipleSizesInMemory()
                .memoryCache(new WeakMemoryCache())
                .memoryCacheSize(2 * 1024 * 1024)
                .discCacheSize(80 * 1024 * 1024)
                .discCacheFileNameGenerator(new Md5FileNameGenerator())
                .tasksProcessingOrder(QueueProcessingType.LIFO)
                .discCacheFileCount(100)
                .defaultDisplayImageOptions(DisplayImageOptions.createSimple())
                .imageDownloader(new BaseImageDownloader(context, 5 * 1000, 30 * 1000))
                .build();

        ImageLoader.getInstance().init(configuration);
        com.nostra13.universalimageloader.utils.L.disableLogging();
    }
}
