package com.nhtzj.learnapplication.activity.base.fragment.listview;


import android.app.Application;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.nhtzj.common.util.XLog;
import com.nhtzj.learnapplication.R;
import com.nhtzj.learnapplication.interf.BaseFragmentInterface;
import com.nhtzj.learnapplication.interf.IDialogControl;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * <pre>
 *     author : Haitao
 *     e-mail : haitao_ni@foxmail.com
 *     time   : 2017/05/17
 *     desc   : 基类 Fragment <br>                已集成butterknife
 *     version: 1.0
 * </pre>
 */

public abstract class BaseFragment extends Fragment implements BaseFragmentInterface {

    //刷新状态，配合BaseListFragment使用
    public static final int STATE_NONE = 0;
    public static final int STATE_REFRESH = 1;
    public static final int STATE_LOADMORE = 2;
    public static final int STATE_NOMORE = 3;
    public static final int STATE_PRESSNONE = 4;// 正在下拉但还没有到刷新的状态
    public static int mState = STATE_NONE;

    protected String TAG;

    //fragment根视图
    public View rootView;
    //当前fragment绑定的acticity
    protected FragmentActivity mHolderActivity;
    //当前实例
    protected Fragment fragment;
    //butterknife实例，用于解绑View，回收资源
    private Unbinder unbinder;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TAG = getClass().getSimpleName();
        mHolderActivity = getActivity();
        fragment = this;
        initData();
        XLog.e("onCreate", TAG);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        XLog.e("onCreateView", TAG);
        rootView = inflater.inflate(getLayoutId(), container, false);
        unbinder = ButterKnife.bind(this, rootView);
        initView(rootView, savedInstanceState);
        bindEvent();
        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentActivity) {
            mHolderActivity = (FragmentActivity) context;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (unbinder != null) {
            unbinder.unbind();
        }
        XLog.e("onDestroyView", TAG);
    }

    /**
     * 显示加载等待视图,显示"加载中..."
     *
     * @return
     */
    protected ProgressDialog showWaitDialog() {
        return showWaitDialog(R.string.loading);
    }

    /**
     * 显示加载等待视图,显示传入字符串资源值
     *
     * @param resid
     * @return
     */
    protected ProgressDialog showWaitDialog(int resid) {
        FragmentActivity activity = getActivity();
        if (activity instanceof IDialogControl) {
            return ((IDialogControl) activity).showWaitDialog(resid);
        }
        return null;
    }

    /**
     * 显示加载等待视图,显示传入字符串
     *
     * @param message
     * @return
     */
    protected ProgressDialog showWaitDialog(String message) {
        FragmentActivity activity = getActivity();
        if (activity instanceof IDialogControl) {
            return ((IDialogControl) activity).showWaitDialog(message);
        }
        return null;
    }

    /**
     * 隐藏加载等待视图
     */
    protected void hideWaitDialog() {
        FragmentActivity activity = getActivity();
        if (activity instanceof IDialogControl) {
            ((IDialogControl) activity).hideWaitDialog();
        }
    }

    /**
     * 获取application实例
     * @return
     */
    public Application getApplication() {
        return getActivity().getApplication();
    }

    /**
     * 获取宿主Activity
     * @return
     */
    protected FragmentActivity getHoldingActivity() {
        return mHolderActivity;
    }

    /**
     * 获取布局文件ID
     * @return
     */
    protected abstract int getLayoutId();


    @Override
    public void initView(View view, Bundle savedInstanceState) {

    }

    @Override
    public void initData() {

    }

    protected void bindEvent() {
    }

}
