package com.nhtzj.learnapplication.activity.sample;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.view.View;
import android.widget.Button;

import com.nhtzj.learnapplication.R;
import com.nhtzj.learnapplication.activity.base.activity.BaseAppCompatActivity;

public class FullScreenActivity extends BaseAppCompatActivity {

    private Button btnChangeOrientation;

    public static void show(Context context) {
        if (context != null) {
            context.startActivity(new Intent(context, FullScreenActivity.class));
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_full_screen;
    }

    @Override
    protected void initView() {
        super.initView();
        btnChangeOrientation = (Button) findViewById(R.id.btn_change_orientation);
    }

    @Override
    protected void bindEvent() {
        super.bindEvent();
        btnChangeOrientation.setOnClickListener(this::onChangeOrientation);
    }

    /**
     * btnChangeOrientation点击事件处理
     *
     * @param v
     */
    private void onChangeOrientation(View v) {
        boolean orientationLandscape = isOrientationLandscape(getResources());
        setRequestedOrientation(orientationLandscape ?
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT : ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
    }
}
