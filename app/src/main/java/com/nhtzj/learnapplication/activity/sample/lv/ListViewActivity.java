package com.nhtzj.learnapplication.activity.sample.lv;

import android.content.Context;
import android.content.Intent;

import com.nhtzj.learnapplication.activity.base.activity.BaseFragmentActivity;
import com.nhtzj.learnapplication.activity.sample.emptyview.EmptyActivity;

/**
 * <pre>
 * author : Haitao
 * blog   : http://blog.nhtzj.com
 * time   : 2018/4/11
 * desc   :
 * version: 2.0
 * </pre>
 */
public class ListViewActivity extends BaseFragmentActivity {
    public static void show(Context context) {
        if (context != null) {
            context.startActivity(new Intent(context, ListViewActivity.class));
        }
    }

    @Override
    protected void initView() {
        super.initView();
        replaceFragment(ListViewFragment.newInstance());
    }
}
