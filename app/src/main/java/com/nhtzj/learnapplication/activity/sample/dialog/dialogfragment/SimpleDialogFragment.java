package com.nhtzj.learnapplication.activity.sample.dialog.dialogfragment;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.nhtzj.learnapplication.R;
import com.nhtzj.learnapplication.util.TDevice;

import java.security.InvalidParameterException;

/**
 * DialogFragment 外部点击事件监听
 */
public class SimpleDialogFragment extends DialogFragment {
    public static final String TAG = "SimpleDialogFragment";

    Builder mBuilder;

    private static SimpleDialogFragment getInstance(Builder builder) {
        if (builder == null) {
            throw new InvalidParameterException("builder is null");
        }
        SimpleDialogFragment dialogFragment = new SimpleDialogFragment();
        dialogFragment.mBuilder = builder;
        return dialogFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //设置弹框属性和显示效动效
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.BottomDialog);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        //自定义Dialog，通过重写onTouchEvent拦截外部点击事件
        OutsideClickDialog outsideClickDialog = new OutsideClickDialog(getContext(), getTheme());
        outsideClickDialog.setOnOutsideClickListener(this::consumeOutsideClick);
        //对返回按键做监听
        outsideClickDialog.setOnKeyListener((dialog1, keyCode, event) -> {
            if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == MotionEvent.ACTION_UP) {
                if (mBuilder.onOutsideClickListener != null) {
                    //根据回调方法判断是否拦截
                    return mBuilder.onOutsideClickListener.consumeOutsideClick();
                } else {
                    return false;
                }
            } else {
                return false;
            }
        });
        return outsideClickDialog;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.view_simple_dialog_fragment, container, true);
        initView(view);
        Dialog dialog = getDialog();
        dialog.setCanceledOnTouchOutside(mBuilder.cancelable);

        Window window = dialog.getWindow();

        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.BOTTOM;
        window.setAttributes(wlp);

        return view;
    }


    @Override
    public void onStart() {
        //处理横屏全屏时显示弹框会显示系统状态栏问题
        Window window = null;
        if (getDialog() != null && (window = getDialog().getWindow()) != null) {
            window.setFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE, WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
        }
        super.onStart();

        if (TDevice.isLandscape(getResources())) {
            TDevice.hideSystemUI(window);
        }
        if (window != null) {
            window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
            window.clearFlags(WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);

            //修改显示宽高
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.WRAP_CONTENT;

            window.setLayout(width, height);
        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (mBuilder.onDismissListener != null) {
            mBuilder.onDismissListener.onDismiss(dialog);
        }
    }

    private void initView(View view) {
        view.findViewById(R.id.btn_close).setOnClickListener(v -> {
            dismiss();
        });
    }

    private boolean consumeOutsideClick() {
        if (mBuilder.onOutsideClickListener != null) {
            //根据回调方法判断是否拦截外部点击事件
            return mBuilder.onOutsideClickListener.consumeOutsideClick();
        }

        return !mBuilder.cancelable;
    }

    public static class Builder {
        boolean cancelable = true;
        private OnOutsideClickListener onOutsideClickListener;
        private DialogInterface.OnDismissListener onDismissListener;


        public Builder setCancelable(boolean cancelable) {
            this.cancelable = cancelable;
            return this;
        }

        public Builder setOutsideClickListener(OnOutsideClickListener onOutsideClickListener) {
            this.onOutsideClickListener = onOutsideClickListener;
            return this;
        }

        public Builder setOnDismissListener(DialogInterface.OnDismissListener onDismissListener) {
            this.onDismissListener = onDismissListener;
            return this;
        }

        public SimpleDialogFragment create() {
            return SimpleDialogFragment.getInstance(this);
        }

        public SimpleDialogFragment show(FragmentManager manager, String tag) {
            if (manager.findFragmentByTag(tag) != null) {
                return (SimpleDialogFragment) manager.findFragmentByTag(tag);
            }
            SimpleDialogFragment dialog = create();
            dialog.show(manager, tag);
            return dialog;
        }
    }

    public static class OutsideClickDialog extends Dialog {

        private boolean mCancelable;

        private OnOutsideClickListener onOutsideClickListener;

        @Override
        public void setCanceledOnTouchOutside(boolean cancel) {
            super.setCanceledOnTouchOutside(cancel);
            mCancelable = cancel;
        }

        public void setOnOutsideClickListener(OnOutsideClickListener onOutsideClickListener) {
            this.onOutsideClickListener = onOutsideClickListener;
        }

        public OutsideClickDialog(@NonNull Context context) {
            super(context);
        }

        public OutsideClickDialog(@NonNull Context context, int themeResId) {
            super(context, themeResId);
        }

        protected OutsideClickDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
            super(context, cancelable, cancelListener);
        }

        @Override
        public boolean onTouchEvent(@NonNull MotionEvent event) {
            if (mCancelable && isShowing() &&
                    (event.getAction() == MotionEvent.ACTION_UP && isOutOfBounds(getContext(), event) ||
                            event.getAction() == MotionEvent.ACTION_OUTSIDE)) {
                boolean consume = onOutsideClickListener.consumeOutsideClick();
                if (consume) {
                    return true;
                }
            }

            return super.onTouchEvent(event);
        }

        /**
         * 判断点击位置是否在DecorView范围内
         * @param context
         * @param event
         * @return
         */
        private boolean isOutOfBounds(Context context, MotionEvent event) {
            final int x = (int) event.getX();
            final int y = (int) event.getY();
            final int slop = ViewConfiguration.get(context).getScaledWindowTouchSlop();
            final View decorView = getWindow().getDecorView();
            return (x < -slop) || (y < -slop)
                    || (x > (decorView.getWidth() + slop))
                    || (y > (decorView.getHeight() + slop));
        }
    }

    public static interface OnOutsideClickListener {
        boolean consumeOutsideClick();
    }
}
