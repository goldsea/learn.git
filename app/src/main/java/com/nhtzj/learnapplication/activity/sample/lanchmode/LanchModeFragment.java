package com.nhtzj.learnapplication.activity.sample.lanchmode;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.nhtzj.learnapplication.R;
import com.nhtzj.learnapplication.activity.base.fragment.listview.BaseFragment;
import com.nhtzj.learnapplication.activity.sample.lanchmode.advance.FlagClearTopActivity;
import com.nhtzj.learnapplication.activity.sample.lanchmode.advance.FlagClearTopSingleTopActivity;
import com.nhtzj.learnapplication.activity.sample.lanchmode.advance.FlagNewTaskActivity;
import com.nhtzj.learnapplication.activity.sample.lanchmode.advance.FlagNewTaskAffinityAActivity;
import com.nhtzj.learnapplication.activity.sample.lanchmode.advance.FlagNewTaskClearTaskActivity;
import com.nhtzj.learnapplication.activity.sample.lanchmode.advance.FlagNewTaskClearTopActivity;
import com.nhtzj.learnapplication.activity.sample.lanchmode.advance.FlagSingleTopActivity;
import com.nhtzj.learnapplication.activity.sample.lanchmode.advance.affinity.AffinitySingleTaskAActivity;
import com.nhtzj.learnapplication.activity.sample.lanchmode.advance.affinity.AffinitySingleTaskBActivity;
import com.nhtzj.learnapplication.activity.sample.lanchmode.advance.affinity.useless.AffinitySingleInstanceActivity;
import com.nhtzj.learnapplication.activity.sample.lanchmode.advance.affinity.useless.AffinitySingleTopAActivity;
import com.nhtzj.learnapplication.activity.sample.lanchmode.advance.affinity.useless.AffinityStandardAActivity;
import com.nhtzj.learnapplication.activity.sample.lanchmode.normal.singleinstance.SingleInstanceActivity;
import com.nhtzj.learnapplication.activity.sample.lanchmode.normal.singletask.SingleTaskActivity;
import com.nhtzj.learnapplication.activity.sample.lanchmode.normal.singletop.SingleTopActivity;
import com.nhtzj.learnapplication.activity.sample.lanchmode.normal.standard.StandardActivity;

import butterknife.BindView;
import butterknife.OnClick;


public class LanchModeFragment extends BaseFragment {

    @BindView(R.id.tv)
    TextView tv;
    @BindView(R.id.btn_standard)
    Button btnStandard;
    @BindView(R.id.btn_singletop)
    Button btnSingletop;
    @BindView(R.id.btn_singletask)
    Button btnSingletask;
    @BindView(R.id.btn_singleinstance)
    Button btnSingleinstance;
    @BindView(R.id.btn_singletask_affinity_a)
    Button btnSingletaskAffinityA;
    @BindView(R.id.btn_singletask_affinity_b)
    Button btnSingletaskAffinityB;
    @BindView(R.id.btn_standard_affinity_a)
    Button btnStandardAffinityA;
    @BindView(R.id.btn_singletop_affinity_a)
    Button btnSingletopAffinityA;
    @BindView(R.id.btn_singleinstance_affinity_a)
    Button btnSingleinstanceAffinityA;
    @BindView(R.id.btn_flag_new_task)
    Button btnFlagNewTask;
    @BindView(R.id.btn_flag_new_task_affinity_a)
    Button btnFlagNewTaskAffinityA;
    @BindView(R.id.btn_flag_new_task_clear_task)
    Button btnFlagNewTaskClearTask;
    @BindView(R.id.btn_flag_single_top)
    Button btnFlagSingleTop;

    public LanchModeFragment() {
        // Required empty public constructor
    }

    public static LanchModeFragment newInstance() {
        return new LanchModeFragment();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_lanch_mode;
    }

    @Override
    public void initView(View view, Bundle savedInstanceState) {
        super.initView(view, savedInstanceState);
        tv.setText(mHolderActivity.toString() + "\nTaskId >>> " + mHolderActivity.getTaskId());
    }

    @OnClick({R.id.btn_standard, R.id.btn_singletop, R.id.btn_singletask, R.id.btn_singleinstance,
            R.id.btn_singletask_affinity_a, R.id.btn_singletask_affinity_b,
            R.id.btn_standard_affinity_a, R.id.btn_singletop_affinity_a, R.id.btn_singleinstance_affinity_a,
            R.id.btn_flag_new_task, R.id.btn_flag_new_task_affinity_a, R.id.btn_flag_new_task_clear_top, R.id.btn_flag_new_task_clear_task, R.id.btn_flag_single_top, R.id.btn_flag_clear_top, R.id.btn_flag_clear_top_single_top
           })
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_standard:
                StandardActivity.show(mHolderActivity);
                break;
            case R.id.btn_singletop:
                SingleTopActivity.show(mHolderActivity);
                break;
            case R.id.btn_singletask:
                SingleTaskActivity.show(mHolderActivity);
                break;
            case R.id.btn_singleinstance:
                SingleInstanceActivity.show(mHolderActivity);
                break;
            case R.id.btn_singletask_affinity_a:
                AffinitySingleTaskAActivity.show(mHolderActivity);
                break;
            case R.id.btn_singletask_affinity_b:
                AffinitySingleTaskBActivity.show(mHolderActivity);
                break;
            case R.id.btn_standard_affinity_a:
                AffinityStandardAActivity.show(mHolderActivity);
                break;
            case R.id.btn_singletop_affinity_a:
                AffinitySingleTopAActivity.show(mHolderActivity);
                break;
            case R.id.btn_singleinstance_affinity_a:
                AffinitySingleInstanceActivity.show(mHolderActivity);
                break;
            case R.id.btn_flag_new_task:
                FlagNewTaskActivity.show(mHolderActivity);
                break;
            case R.id.btn_flag_new_task_affinity_a:
                FlagNewTaskAffinityAActivity.show(mHolderActivity);
                break;
            case R.id.btn_flag_new_task_clear_top:
                FlagNewTaskClearTopActivity.show(mHolderActivity);
                break;
            case R.id.btn_flag_new_task_clear_task:
                FlagNewTaskClearTaskActivity.show(mHolderActivity);
                break;
            case R.id.btn_flag_single_top:
                FlagSingleTopActivity.show(mHolderActivity);
                break;
            case R.id.btn_flag_clear_top:
                FlagClearTopActivity.show(mHolderActivity);
                break;
            case R.id.btn_flag_clear_top_single_top:
                FlagClearTopSingleTopActivity.show(mHolderActivity);
                break;

        }
    }

    public void onNewIntent() {
        if (tv != null) {
            tv.append("\nonNewIntent");
        }
    }

}
