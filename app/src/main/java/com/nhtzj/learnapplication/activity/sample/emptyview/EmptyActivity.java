package com.nhtzj.learnapplication.activity.sample.emptyview;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.AppCompatButton;
import android.view.View;

import com.nhtzj.learnapplication.R;
import com.nhtzj.learnapplication.activity.base.activity.BaseAppCompatActivity;
import com.nhtzj.learnapplication.widget.empty.EmptyLayout;

import butterknife.BindView;
import butterknife.OnClick;

public class EmptyActivity extends BaseAppCompatActivity implements EmptyLayout.OnViewClick {

    @BindView(R.id.emptylayout)
    EmptyLayout emptylayout;
    @BindView(R.id.btn_loading)
    AppCompatButton btnLoading;
    @BindView(R.id.btn_error)
    AppCompatButton btnError;
    @BindView(R.id.btn_empty)
    AppCompatButton btnEmpty;
    @BindView(R.id.btn_hide)
    AppCompatButton btnHide;

    public static void show(Context context) {
        if (context != null) {
            context.startActivity(new Intent(context, EmptyActivity.class));
        }
    }


    @Override
    protected int getLayoutId() {
        return R.layout.activity_empty;
    }

    @Override
    protected void bindEvent() {
        super.bindEvent();
        emptylayout.setOnLayoutClickListener(this);
    }

    @OnClick({R.id.btn_loading, R.id.btn_error, R.id.btn_empty, R.id.btn_hide})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_loading:
                emptylayout.setErrorType(EmptyLayout.NETWORK_LOADING);
                break;
            case R.id.btn_error:
                emptylayout.setErrorType(EmptyLayout.NETWORK_ERROR);
                break;
            case R.id.btn_empty:
                emptylayout.setErrorType(EmptyLayout.NODATA);
                break;
            case R.id.btn_hide:
                emptylayout.setErrorType(EmptyLayout.HIDE_LAYOUT);
                break;
        }
    }

    @Override
    public void onViewClick(View v) {
        emptylayout.setErrorType(EmptyLayout.NETWORK_LOADING);
    }
}
