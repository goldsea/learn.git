package com.nhtzj.learnapplication.activity.sample;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.widget.TextView;

import com.nhtzj.common.util.SizeUtils;
import com.nhtzj.learnapplication.R;
import com.nhtzj.learnapplication.activity.base.activity.BaseAppCompatActivity;

public class TextViewActivity extends BaseAppCompatActivity {

    public static void show(Context context) {
        if (context != null) {
            context.startActivity(new Intent(context, TextViewActivity.class));
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_text_view;
    }

    @Override
    protected void initView() {
        super.initView();
        //等同于 android:lineHeight="30dp"
        TextView tvLineHeight = findViewById(R.id.tv_line_height);
        int lineHeight = SizeUtils.dp2px(30);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            tvLineHeight.setLineHeight(lineHeight);
        }

        //等同于android:lineSpacingExtra="10dp"
        TextView tvLineSpacingExtra = findViewById(R.id.tv_line_spacing_extra);
        int add = SizeUtils.dp2px(10);
        tvLineSpacingExtra.setLineSpacing(add, 1);

        //等同于 android:lineSpacingMultiplier="1.5"
        TextView tvLineSpacingMultiplier = findViewById(R.id.tv_line_spacing_multiplier);
        tvLineSpacingMultiplier.setLineSpacing(0, 1.5f);

        //等同于   android:letterSpacing="0.5"
        TextView tvLetterSpace = findViewById(R.id.tv_letter_space);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            tvLetterSpace.setLetterSpacing(0.5f);
        }
    }
}
