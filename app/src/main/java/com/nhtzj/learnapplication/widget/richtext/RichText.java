package com.nhtzj.learnapplication.widget.richtext;


import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.AppCompatTextView;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ImageSpan;
import android.text.style.URLSpan;
import android.util.AttributeSet;
import android.view.View;

import com.nhtzj.learnapplication.R;
import com.nhtzj.learnapplication.util.TDevice;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

import java.util.ArrayList;
import java.util.List;

/**
 * <pre>
 * author : Haitao
 * blog   : http://www.nhtzj.com
 * time   : 2018/5/21
 * desc   : 简单富文本
 * version: 2.0
 * </pre>
 */
public class RichText extends AppCompatTextView {

    private ImageLoader mLoader;
    private DisplayImageOptions options;

    private int screenWidth;
    private int viewWidth;

    private int widthExp;

    private Drawable placeHolder, errorImage;//占位图、出错图
    private OnImageClickListener onImageClickListener;//图片点击回调
    private OnUrlClickListener onUrlClickListener;//URL点击回调

    public RichText(Context context) {
        this(context, null);
    }

    public RichText(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RichText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.RichText);
        placeHolder = typedArray.getDrawable(R.styleable.RichText_placeHolder);
        errorImage = typedArray.getDrawable(R.styleable.RichText_errorImage);


        if (placeHolder == null) {
            placeHolder = new ColorDrawable(Color.GRAY);
        }
        if (errorImage == null) {
            errorImage = new ColorDrawable(Color.GRAY);
        }
        typedArray.recycle();

        mLoader = ImageLoader.getInstance();
        options = new DisplayImageOptions.Builder()
                .showImageForEmptyUri(R.drawable.icon_pic_rec)
                .showImageOnFail(R.drawable.icon_pic_rec)
                .resetViewBeforeLoading(true)
                .cacheOnDisc(true)
                .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .displayer(new FadeInBitmapDisplayer(300))
                .build();

        screenWidth = (int) TDevice.getScreenWidth();
        viewWidth = (int) (screenWidth - TDevice.dp2px(15));
        screenWidth = (int) (screenWidth - TDevice.dp2px(25));

        //设置Textview内的链接可点击，且Textview可滑动
        setMovementMethod(LinkMovementMethod.getInstance());
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);


        if (widthMode == MeasureSpec.EXACTLY) {
            // Parent has told us how big to be. So be it.
            widthExp = widthSize;
        } else {
            widthExp = widthSize;
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.setText(null);
    }

    /**
     * 设置要显示的富文本内容
     *
     * @param text 内容
     */
    public void setRichText(String text) {
        //使用Html.fromHtml解析标签，
        //asyncImageGetter用于加载img标签图片资源
        Spanned spanned = Html.fromHtml(text, asyncImageGetter, null);
        SpannableStringBuilder spannableStringBuilder;
        if (spanned instanceof SpannableStringBuilder) {
            spannableStringBuilder = (SpannableStringBuilder) spanned;
        } else {
            spannableStringBuilder = new SpannableStringBuilder(spanned);
        }

        //获取a标签，设置点击事件及回调
        URLSpan[] urls = spannableStringBuilder.getSpans(0, spannableStringBuilder.length(), URLSpan.class);
        for (URLSpan span : urls) {
            setLinkClickable(spannableStringBuilder, span);
        }

        //获取img标签，设置图片的点击事件及回调
        ImageSpan[] imageSpans = spannableStringBuilder.getSpans(0, spannableStringBuilder.length(), ImageSpan.class);
        final List<String> imageUrls = new ArrayList<>();

        for (int i = 0, size = imageSpans.length; i < size; i++) {
            ImageSpan imageSpan = imageSpans[i];
            final String imageUrl = imageSpan.getSource();
            int start = spannableStringBuilder.getSpanStart(imageSpan);
            int end = spannableStringBuilder.getSpanEnd(imageSpan);
            imageUrls.add(imageUrl);

            final int iFinal = i;
            ClickableSpan clickableSpan = new ClickableSpan() {
                @Override
                public void onClick(View widget) {
                    if (onImageClickListener != null) {
                        onImageClickListener.imageClicked(imageUrls, iFinal);
                    }
                }
            };

            //去除在图片间默认的点击效果
            ClickableSpan[] clickableSpans = spannableStringBuilder.getSpans(start, end, ClickableSpan.class);
            if (clickableSpans != null && clickableSpans.length != 0) {
                for (ClickableSpan cs : clickableSpans) {
                    spannableStringBuilder.removeSpan(cs);
                }
            }

            spannableStringBuilder.setSpan(clickableSpan, start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }

        super.setText(spanned);
    }

    /**
     * 设置链接点击回调
     *
     * @param clickableHtmlBuilder
     * @param urlSpan
     */
    private void setLinkClickable(SpannableStringBuilder clickableHtmlBuilder, URLSpan urlSpan) {
        int start = clickableHtmlBuilder.getSpanStart(urlSpan);
        int end = clickableHtmlBuilder.getSpanEnd(urlSpan);
        int flags = clickableHtmlBuilder.getSpanFlags(urlSpan);
        final String url = urlSpan.getURL();
        ClickableSpan clickableSpan = new ClickableSpan() {
            public void onClick(View view) {
                onUrlClickListener.urlClicked(url);
            }
        };
        //将原有链接点击效果去除，因为TextView会调用URLSpan.onClick()方法进行网页跳转
        clickableHtmlBuilder.removeSpan(urlSpan);
        clickableHtmlBuilder.setSpan(clickableSpan, start, end, flags);
    }

    /**
     * 加载图片资源
     */
    private Html.ImageGetter asyncImageGetter = new Html.ImageGetter() {
        @Override
        public Drawable getDrawable(String source) {
            final URLDrawable urlDrawable = new URLDrawable(getContext());

            if (source.contains("storage/emulated") || source.toLowerCase().contains("/sdcard/")) {
                source = "file://" + source;
            } else if (source.startsWith("http://")) {
//                source = source;
            } else {
                source = "http://www.nhtzj.com" + source;
            }

            mLoader.loadImage(source, options, new SimpleImageLoadingListener() {


                @Override
                public void onLoadingStarted(String imageUri, View view) {
                    urlDrawable.setBounds(placeHolder.getBounds());
                    urlDrawable.setDrawable(placeHolder);
                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                    urlDrawable.setBounds(errorImage.getBounds());
                    urlDrawable.setDrawable(errorImage);
                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    super.onLoadingComplete(imageUri, view, loadedImage);

                    int width;
                    int height;

                    //Bitmap转成Drawable
                    Drawable drawable = new BitmapDrawable(getContext().getResources(), loadedImage);

                    float imgH = (float) loadedImage.getHeight();
                    float imgW = (float) loadedImage.getWidth();

                    //根据需求调整图片大小，
                    //我这边是将图片调整为适应屏幕宽度

//                    if (imgW > viewWidth) {
                    width = viewWidth;
                    height = (int) (width * (imgH / imgW));
//                    } else {
//                        width = (int) imgW;
//                        height = (int) imgH;
//                    }
                    drawable.setBounds(0, 0, width, height);
                    urlDrawable.setBounds(0, 0, screenWidth, height + 5);
                    urlDrawable.setDrawable(drawable);
                    RichText.this.setText(getText());
                }

                @Override
                public void onLoadingCancelled(String imageUri, View view) {
                    super.onLoadingCancelled(imageUri, view);
                    urlDrawable.setBounds(errorImage.getBounds());
                    urlDrawable.setDrawable(errorImage);
                }
            });


            return urlDrawable;
        }
    };

    /**
     * 缩放图片
     * @param bitmap
     * @param scale
     * @return
     */
    private static Bitmap small(Bitmap bitmap, float scale) {
        Matrix matrix = new Matrix();
        matrix.postScale(scale, scale); //长和宽放大缩小的比例
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

    public static class URLDrawable extends BitmapDrawable {
        private Drawable drawable;

        public URLDrawable(Context context) {
//			drawable = context.getResources().getDrawable(R.drawable.icon_pic_rec);
        }

        @Override
        public void draw(Canvas canvas) {
            if (drawable != null)
                drawable.draw(canvas);
        }

        public void setDrawable(Drawable drawable) {
            this.drawable = drawable;
        }
    }

    /**
     * 设置加载等待图
     *
     * @param placeHolder
     */
    public void setPlaceHolder(Drawable placeHolder) {
        this.placeHolder = placeHolder;
    }

    /**
     * 设置加载错误图
     *
     * @param errorImage
     */
    public void setErrorImage(Drawable errorImage) {
        this.errorImage = errorImage;
    }

    /**
     * 设置图片点击回调
     *
     * @param onImageClickListener
     */
    public void setOnImageClickListener(OnImageClickListener onImageClickListener) {
        this.onImageClickListener = onImageClickListener;
    }

    /**
     * 设置链接点击回调
     *
     * @param onUrlClickListener
     */
    public void setOnUrlClickListenerListener(OnUrlClickListener onUrlClickListener) {
        this.onUrlClickListener = onUrlClickListener;
    }

    public interface OnImageClickListener {
        void imageClicked(List<String> imageUrls, int position);
    }

    public interface OnUrlClickListener {
        void urlClicked(String url);
    }

}
